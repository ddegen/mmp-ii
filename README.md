# Methoden der Mathematischen Physik II

### Inhalt
Im Zentrum dieses Projektes steht eine Prüfungszusammenfassung aus dem zweiten
Jahr des Physik Bachelor Kurses "MMP II" der ETH Zürich, gehalten von Prof. T.
Willwacher im Frühjahrssemester 2019.

### Disclaimer
Wir können weder Vollständigkeit noch Fehlerfreiheit der Inhalte garantieren.
Zögere nicht uns zu kontaktieren, falls Du weitere Vorschläge hast oder
Dir grammatikalische, mathematische Fehler bzw. Ungenauigkeiten auffallen
sollten.

### Compiliere es selbst!
Um die `.tex` Datei MMP II in ein PDF zu übersetzen, stelle sicher, dass Du
LaTeX installiert hast und erteile im jeweiligen Projekt Ordner folgenden
Befehl:

`latexmk -pdf *tex`

Um die durch das Kommando erzeugten Hilfs-Dateien zu entfernen, benutze:

`latexmk -c`

Die meisten LaTeX Distribution sind standardmässig mit diesem Kompilator
versehen.
