\chapter{Darstellung von (endlichen) Gruppen}

\section{Definitionen}

\begin{definition}
  Sei G eine Gruppe und V ein $\mathbb{K}$-Vektorraum. Die \textbf{Darstellung}
  von G auf V ist ein Gruppenhomomorphismus $\rho$: $G \rightarrow GL(V)$.
\end{definition}

\begin{remark}
  Eine Darstellung ist eine Wirkung auf Vektorräumen über lineare Abbildungen.
\end{remark}

\begin{lemma}
  Sei $\rho : G \rightarrow GL(V)$ eine Darstellung. Dann ist $\rho(1) = 1_V$,
  und $\rho(g)^{-1} = \rho(g^{-1})$.
\end{lemma}

\begin{definition}
  Sei $\rho$ eine Wirkung von G auf einen Vektorraum V. Die \textbf{duale
  Darstellung} $\rho^*$ auf $V^*$ ist für $g \in G$, $l \in V^*$ und $x \in V$
  gegeben durch:
  \begin{equation*}
    (\rho^*_g (l))(x) \coloneqq l(\rho_{g^{-1}}x)
  \end{equation*}
\end{definition}

\begin{corollary}
Die duale Darstellung kann auch geschrieben werden als $\rho^*_g = \left( \rho(g)^{-1} \right) ^T$. 
\end{corollary}

\begin{definition}
  Die \textbf{reguläre Darstellung} von $G$ ist die Darstellung von G auf
  $\mathbb{C}[G]$ mittels Linksmultiplikation $\rho (g) \cdot x = gx$.
\end{definition}

\begin{definition}
Gegeben sind zwei Darstellungen $\rho, \rho^\prime$ von G auf zwei Vektorräume
  $V , V^\prime$. Sei $v \in V$, $v^\prime \in V^\prime$. Wir definieren folgende
  speziellen Darstellungen: \\
  $\bullet$ Die \textbf{direkte Summen Darstellung}
    $\rho \bigoplus \rho^\prime$ auf $V \bigoplus V^\prime$ wobei
    \begin{equation*}
      \left( \rho \oplus \rho ^ { \prime } \right) ( g ) \left( v + v ^ { \prime }
      \right) : = \rho ( g ) ( v ) + \rho ^ { \prime } ( g ) \left( v ^ { \prime }
      \right)
    \end{equation*}
  $\bullet$ Die \textbf{Tensorprodukt Darstellung} $\rho \bigotimes \rho^\prime$
    auf $V \bigotimes V^\prime$ wobei
    \begin{equation*}
        \left( \rho \otimes \rho ^ { \prime } \right) ( g ) \left( v \otimes v ^ {
      \prime } \right) : = \rho ( g ) ( v ) \otimes \rho ^ { \prime } ( g ) \left(
      v ^ { \prime } \right)
    \end{equation*}
  $\bullet$ Die \textbf{Homomorphismus Darstellung} auf Hom(V,$V^\prime$) wobei
    \begin{equation*}
      \rho _ { \mathrm { Hom } } ( g ) f : = \rho ^ { \prime } ( g ) \circ f \circ
      \rho \left( g ^ { - 1 } \right)
    \end{equation*}
\end{definition}

\begin{definition}
  Zwei Darstellungen $(\rho_1,V_1),(\rho_2,V_2)$ sind \textbf{äquivalent} oder
  auch \textbf{isomorph} falls ein bijektiver Homomorphismus von Darstellungen
  $\varphi$: $V_1 \rightarrow V_2$ existiert. \\
  Der Vektorraum aller \textbf{Homomorphismen von Darstellungen} $(\rho_1,V_1) \rightarrow (\rho_2,V_2)$
  wird mit $Hom_G(V_1,V_2)$ oder $Hom_G(\rho_1,V_1),(\rho_2,V_2)$ bezeichnet. Elemente aus diesem Raum nennt man \textbf{intertwiner} oder \textbf{equivariant}.
  Allgemein gilt:
  \begin{equation*}
    \forall f \in Hom_G(V_1,V_2) : (\forall g \in G: f\circ \rho_1(g) = \rho_2 (g)
    \circ f)
  \end{equation*}
\end{definition}

\section{Irreduzible und vollständig irreduzible Darstellungen}
\begin{definition}
  Ein Unterraum $W \subset V$
  ist \textbf{invariant} unter der Darstellung ($\rho$,V), falls
  $\rho(g) W \subset W$ $\forall g \in G$.
\end{definition}

\begin{definition}
  Eine Darstellung ($\rho$,V) ist \textbf{irreduzibel}, falls neben $V$ und
  {$0$} keine invarianten Unterräume existieren.
  Gilt dies \ul{nicht}, so ist ($\rho$,V)  \textbf{reduzibel}.
\end{definition}

\begin{definition}
  Eine \textbf{Unterdarstellung} ist gegeben durch die Einschränkung einer Darstellung $(\rho, V)$
  auf einen nichttrivialen Unterraum $W$ $\rho |_W$:
  $G \rightarrow GL(W), g \longmapsto \rho_g|_W$ .
\end{definition}

\begin{definition}
  Die Darstellung ($\rho$,V) ist \textbf{vollständig reduzibel} falls sie in
  die Direkte Summe irreduzibler Darstellungen zerfällt. Dh. falls invariante
  Unterräume $V_1,\ldots,V_n$ existieren, so dass $V= V_1 \bigoplus \ldots
  \bigoplus V_n$ und die Unterdarstellung $(\rho|_{V_i},V_i)$ irreduzibel sind.
\end{definition}

\begin{lemma}
  G endl. Gruppe, $\rho :G \rightarrow GL(V)$ irreduzible
  Darstellung $\Rightarrow \operatorname{dim}(V)<\infty$.
\end{lemma}

\section{Unitäre Darstellungen}
\begin{definition}
  Eine Darstellung $\rho$ auf einem Vektorraum V mit Skalarprodukt heisst
  \textbf{unitär}, falls $\forall g \in G$ $\rho(g)$ unitär ist. D.h.
  $\rho(g)^* = \rho(g)^{-1} = \rho(g^{-1})$.
\end{definition}

\begin{lemma}
  Sei $V$ eine unitäre Darstellung von G und $W\subset V$ ein invarianter
  Unterraum. Dann ist auch $W^{\perp}$ auch invariant.
\end{lemma}

\begin{corollary}
  Unitäre Darstellungen sind vollständig reduzibel.
\end{corollary}

\begin{lemma}
  Sei $V$ eine Darstellung einer endlichen Gruppe G und sei
  $\inner{\cdot}{\cdot}$ ein Skalarprodukt auf V. Dann ist die Darstellung unitär
  bezüglich dem gemittelten Skalarprodukt
  \begin{equation*}
    \inner{v}{w}_G\defeq\frac{1}{|G|}\sum_{g\in G}\inner{\rho_g v}{\rho_g w}.
  \end{equation*}
\end{lemma}

\begin{corollary}
  Darstellungen von endl. bzw. kompakten Gruppen sind vollständig reduzibel.
\end{corollary}

\section{Das Lemma von Schur}
\begin{lemma}\textbf{Das Lemma von Schur}\\
  Seien $(\rho_1,V_1),(\rho_2,V_2)$ irreduzible endlichdimensionale
  Darstellungen von G.
  \begin{enumerate}[(i)]
    \item $\varphi \in Hom_G(V_1,V_2) \Rightarrow \varphi \equiv 0$ oder
      $\varphi$ ist ein Isomorphismus.
  \end{enumerate}
  Ist darüberhinaus $V_1$ ein komplexer Vektorraum, so existiert ein Eigenwert,
  da das charakteristische Polynom zerfällt und es gilt
  \begin{enumerate}[(ii)]
    \item $\varphi \in
      Hom_G(V_1,V_1)$. Dann ist $\varphi = \lambda Id, \lambda \in \mathbb{C}$.
  \end{enumerate}
\end{lemma}

\begin{proof}[Idee]
  Verwende, dass $\im (\phi)$ und $\Ker(\phi)$ invariante Unterräume ($v \in Ker(\phi) \rightarrow \rho_1(g)v=0$) von $\rho_1$, $\rho_2$ sind, also, da $V_i$ irred., gleich $0$ oder $V$.
\end{proof}

\begin{corollary}
  Jede endlich dimensionale irreduzible komplexe Darstellung einer abelschen
  Gruppe ist 1 Dimensional.
\end{corollary}

\section{Charaktere und die Orthogonalität der Matrixelemente}
Im folgenden werden wir nur Darstellungen über den Körper $\K\in\{\R,\C\}$
betrachten.
\begin{definition}
  Sei $\rho : G \rightarrow GL(V)$ eine Darstellung der Gruppe G auf dem
  Vektorraum V. Der \textbf{Charakter von $\rho_V$}, ist die Abbildung
   $\chi_V\colon g\in G \mapsto \chi_V(g)=\tr(\rho_g)\in \K$
\end{definition}

\begin{theorem}
  Für den Charakter $\chi_\rho$ gelten folgende Aussagen:
  \begin{enumerate}[(i)]
    \item $\chi_\rho (g) = \chi_\rho (hgh^{-1})$
    \item Sind $\rho, \rho^\prime$ äquivalente Darstellungen, so gilt $\chi_\rho
      = \chi_{\rho^\prime}$.
    \item $\chi_\rho (1) = dim(V)$.
    \item $\chi_{\rho \oplus \rho^\prime} = \chi_{V \oplus V^\prime} = \chi_\rho
      + \chi_{\rho^\prime} = \chi_V + \chi_{V^\prime}$.
    \item $\chi_\rho (g^{-1}) = \overline{\chi_\rho (g)}, \forall g \in G$.
    \item $\chi_{\rho \otimes \rho^\prime} = \chi_{V \otimes V^\prime} =
      \chi_\rho \cdot \chi_{\rho^\prime} = \chi_V \cdot \chi_{V^\prime}$.
    \item $\chi_{V^*}(g) = \chi_{\rho^*}(g) = \overline{\chi_V (g)} =
      \overline{\chi_\rho (g)}$.
  \end{enumerate}
\end{theorem}

Betrachte den Raum der Funktionen $G\rightarrow\C$, $L^2(G)\cong\C^{|G|}$
mit dem Skalarprodukt
\begin{equation}\label{eq:inner_L2G}
  \inner{f}{f'}\coloneqq\frac{1}{|G|}\sum_{g\in G}\overline{f(g)}f'(g).
\end{equation}

\begin{definition}
  Eine Funktion $F: G\rightarrow \mathbb{C}$ heisst \textbf{Klassenfunktion},
  falls
  \begin{equation*}
    f(ghg^{-1})=f(h) \quad \forall g,h \in G
  \end{equation*}
  Wir bezeichnen mit $L^2(G)^G$ den Raum der
  Klassenfunktionen $G\rightarrow\C$. Für $f,f'\in L^2(G)^G$ vereinfacht sich das
  Skalarprodukt mit $C_j$ den Konjugationsklassen von $G$ zu
  \begin{equation*}
    \inner{f}{f'} = \frac{1}{|G|} \sum_{j = 1}^m |(C_j)| \overline{f(C_j)}f'(C_j),
  \end{equation*}
\end{definition}
\begin{corollary}
  Sei G eine endl. Gruppe. Die Charaktere $\chi_1,\ldots,\chi_k$ der irreduzieblen Darstellungen von G 
  bilden eine ONB des Hilbertraums der Klassenunktionen.
\end{corollary}
\begin{definition}
  Für eine Darstellung $\rho\colon G\rightarrow \GL(V)$, eine Basis
  von V und geeignete Indizes $i,j$ bezeichnen wir mit den \textbf{Matrixelementen} $\rho_{ij}$
  , die Abbildung $(g\mapsto(\rho_g)_{ij})\in L^2(G)$.
\end{definition}

\begin{theorem}
  \textbf{Orthogonalitätsrelationen}\\
  Seien $\rho \colon G \rightarrow GL(V), p^\prime \colon G \rightarrow
  GL(V^\prime)$ irreduzible unitäre Darstellungen einer endlichen Gruppe $G$.
  Bezüglich zwei beliebig gewählter ONBs von $V$ bzw. $V^\prime$ gilt $\forall
  i,j,k,l$
  \begin{enumerate}[(i)]
    \item Falls $\rho,\rho'$ inäquivalent
$
          \inner{\rho_{ij}}{\rho'_{kl}} = \frac{1}{|G|} \sum_{g \in G}
          \overline{\rho_{ij}(g)}\rho'_{kl}(g) = 0
 $
    \item
  $
        \inner{\rho_{ij}}{\rho_{kl}}=\frac{1}{|G|} \sum_{g \in G}
        \overline{\rho_{ij}(g)}\rho_{kl}(g) =
        \frac{1}{dim(V)}\delta_{ik}\delta_{jl}
   $
  \end{enumerate}
\end{theorem}

\begin{lemma}
  Seien $V$ und $V'$ und deren ONB wie oben. Für festes $i$ und $k$ ist die
  Matrix $M_{ik}$ mit Einträgen
  \begin{equation*}
    (M_{ik})_{jl}=\frac{1}{|G|} \sum_{g \in G}
    \overline{\rho_{ij}(g)}\rho'_{kl}(g)
  \end{equation*}
  ein Homomorphismus von Darstellungen $M_{ik}\in\hom(V,V').$
\end{lemma}
\begin{proof}[Idee, für Orthogonalitätsrelationen]
  Metzge $M_{ik}$ mit Schur. (i) ist klar und für (ii) zeige man, dass
  $(M_{ik})_{jl}=(M_{jl})_{ik}$ gilt, um
  $(M_{ik})_{jl}=\lambda\delta_{ik}\delta_{jl}$ zu zeigen. Danach summiere über
  $i$ bei $k=i$ und festem $j,l$. Zeige $\lambda=1/\dim(V)$.
\end{proof}

\begin{lemma}
  Sei $(\rho,V)$ eine endlichdimensionale Darstellung so dass zu jedem
  invarianten Unterraum $W \subset V$ ein invarianter Unterraum $W^\prime$
  existiert mit $V = W \bigoplus W^\prime$. Dann ist $(\rho,V)$ vollständig
  reduzibel.
\end{lemma}

\begin{corollary}[Erste Orthogonalitätsrelation der
  Charaktere]\label{kor:erste_char_orth}
  Seien $\rho,\rho^\prime$ irreduzible Darstellungen der endlichen Gruppe $G$
  und $\chi_\rho, \chi_{\rho^\prime}$ ihre Charaktere. Dann gilt
  \begin{equation*}
    \inner{\chi_{\rho}}{\chi_{\rho'}}=\left\{
      \begin{matrix*}[l] 0, & \text{falls }\rho,\rho' \text{ inäquivalent} \\
        1, &\text{sonst}
      \end{matrix*}\right.
\end{equation*}
\end{corollary}

\begin{corollary}
  Weil die Charaktere Klassenfunktionen sind, d.h. $\chi\in L^2(G)^G$ und $\dim
  L^2(G)^G=$\#(Konjugationsklassen von $G$), ist die Anzahl inäquivalenter
  irred. Darstellungen von $G$ kleiner als die Anzahl Konjugationsklassen von
  $G$.
\end{corollary}


\begin{definition}\label{def:isotyp_komp}
  Die kanonische Zerlegung in \textbf{isotypische Komponenten} $W_\lambda$ ist definiert als:
  \begin{equation*}
    V \cong \ldots \bigoplus\underbrace{V_{\lambda} \bigoplus
    \ldots \bigoplus V_\lambda}_{n_\lambda \text{ mal}} \bigoplus \ldots \cong
    \bigoplus_{\lambda}(V_{\lambda}\otimes\C^{n_{\lambda}})
    \coloneqq \overset{n}{\underset{\lambda = 1}{\bigoplus}}W_\lambda
  \end{equation*}
  wobei die Summe über die irred. Darstellungen $\lambda$ geht und $n_{\lambda}$
  die Vielfachheit der jeweiligen irred. Darstellung ist.
\end{definition}

\begin{remark}\label{rem:vfh_isotyp}
 Für die Zerlegung einer endl. dim Darstellung $V$ in isotypische Komponenten $W_{\lambda}$ wie in Definition \ref{def:isotyp_komp} gilt einerseits $\chi_V=\sum_{\lambda}n_{\lambda}\chi_{V_{\lambda}}$ und andererseits $\inner{\chi_V}{\chi_{V_{\lambda}}}=n_{\lambda}$ (Beweis: Verwende Korollar \ref{kor:erste_char_orth}). Weiter entspricht wegen Linearität von $\inner{}{}$ der Ausdruck $\inner{\chi_V}{\chi_V}=\#\{\text{irred. Unterdarstellungen von }V\}$. \textbf{Ist also $\inner{\chi_V}{\chi_V}=1$, so ist $V$ irreduzibel}.
\end{remark}

\begin{proposition}
Die \textbf{kanonische Projektion} auf die isotypischen Komponenten ist orthogonal:
$$p_j \coloneqq \operatorname{dim}(V_j) \sum_{g \in G} \overline{\chi_j (g)}\rho(g)$$
\end{proposition}
\begin{proof}[Idee]
  Um z.z. dass es wirklich eine orthogonale Projekton ist. $\forall g \in G$ gilt:
  \begin{equation*}
    \rho(g)=\bigoplus_{\lambda=1}^n\rho_{\lambda}(g)\otimes\mathbb{1}_{n_\lambda\times
    n_\lambda}.
  \end{equation*}
  Wählen wir nun für jedes $V_\lambda$ noch eine ONB, so zeigt eine explizite
  Rechnung, dass $p_\lambda$ in der Tat die gesuchte orthogonale Projektion ist.
\end{proof}
\begin{corollary}
Es gilt, dass $p_j$ selbst-adjungiert ist ($p_j^* = p_j$) und aus $p_i(p_j(v))=0$ folgt direkt, dass $W_i \perp W_j$.
\end{corollary}
\section{Existenz und Klassifikation irreduzibler Darstellungen}
\begin{definition}
  Eine $\mathbb{K}$-\textbf{Algebra} ist ein $\mathbb{K}$-Vektorraum mit einer
  bilinearen Abbildung, der sog. inneren Multiplikation, $\mu: A \times A
  \rightarrow A $ und einem Einselement $\mathbb{1}\in\K$ so dass gilt:
  \begin{itemize}
    \item (Assoziativität)$\forall a,b,c \in A: \mu(a,\mu(b,c)) =
      \mu(\mu(a,b),c) \Longleftrightarrow (ab)c = A(bc)$.
    \item (Eins-Element) $\forall a \in A : \mu(\mathbb{1},a)= \mu(a,\mathbb{1})
      = a$.
  \end{itemize}
\end{definition}

\begin{definition}
  Bei einer \textbf{Gruppenalgebra} $\mathbb{K}[G]=\span{G}_{\K}$ bilden
  die Gruppenelemente eine $\mathbb{K}$-Basis des $\mathbb{K}$-Vektorraums,
  die Multiplikation ist gegeben durch die bilineare Fortsetzung der
  Gruppenmultiplikation:
  \begin{equation*}
    \left( \sum _ { g \in G } c _ { g } g \right) \left( \sum _ { h \in G } d _
    { h } h \right) : = \sum _ { g \in G , h \in H } c _ { g } d _ { h } ( g h )
    = \sum _ { g \in G } \left( \sum _ { h \in H } c _ { g h } - d _ { h }
    \right) g \qquad c _ { g }, \, d _ { h } \in \mathbb { K }
  \end{equation*}
  Die Elemente sind eine formale $\K$-Linearkombination aus Elementen von $G$
  \begin{equation*}
    \K[G]=\left\{\sum_{g\in G}c_g g \mid c_g\in\K \right\}.
  \end{equation*}
\end{definition}


\begin{theorem}
  Zerlegen wir die reguläre Darstellung $\C[G]$ in invariante Unterdarstellungen
  wie in Definition \ref{def:isotyp_komp}, so gilt $n_\lambda = dim(V_\lambda)$.
\end{theorem}
\begin{proof}[Idee]
  Verwende einerseits Bemerkung \ref{rem:vfh_isotyp} und zeige explizit
  $\chi(g)=1$ genau dann wenn $g=\mathbb{1}$. Schliesslich verwende, dass
  $\chi_{\lambda}(1)=\dim V_{\lambda}$.
\end{proof}

\begin{corollary}
  Seien $V_1,\ldots,V_m$ die (Isomorphismusklassen der) irreduziblen
  Darstellungen von G. Dann gilt:
  \begin{equation*}
    |G| = \sum_{j=1}^m (dim(V_j))^2
  \end{equation*}
\end{corollary}

\begin{corollary}
  \textbf{Peter-Weyl Theorem, endliche Version}\\
  Die Matrix-Elemente der Darstellungsmatrizen der irreduzieblen Darstellung
  bezüglich der orthonormal Basis bilden eine ONB von $L^2(G)$.
\end{corollary}

\begin{corollary}[Maschke]
  Sei $V_1,\ldots,V_m$ eine irreduzieble, komplexe
  Darstellung der endliche Gruppe G. Dann ist
  \begin{equation*}
    \mathbb{C}[G] \rightarrow \overset{m}{\underset{j=1}{\bigoplus}}
    Hom_{\C}(V_j,V_j)
    \qquad \text{ bzw. } \qquad
    g\mapsto
      \begin{bmatrix}
        \rho_1(g) & 0 & \cdots & 0\\
        0 & \rho_2(g) &  & \vdots \\
        \vdots &  & \ddots & 0\\
        0 & \cdots & 0 & \rho_m(g)
      \end{bmatrix}
   \end{equation*}
  Somit ist dies ein Darstellungsisomorphismus bezüglich Rechts- und
  Linksmultiplikation.
\end{corollary}

\begin{definition}
  Die \textbf{adjungierte Darstellung} von G auf $\C[G]$ ist
  \begin{equation*}
  \rho^{\text{adj}}(g)x = gxg^{-1}, \quad \mathbb{C}[G]_{\text{adj}} \cong \underset{\lambda}{\bigoplus} V_\lambda
    \otimes C^{n_\lambda^{\text{adj}}}
  \end{equation*}
\end{definition}

\begin{definition}
  Bei \textbf{Gruppenwirkung von $G\acts S$}, definiere
  $S^G\defeq\{s\in S\mid g\cdot s = s, \forall g\in G\}$.
\end{definition}

\begin{theorem}
  Die Anzahl der (komplexen) irreduziblen Darstellungen einer endlichen Gruppe
  $G$ ist gleich der Anzahl Konjugationsklassen von $G$.
\end{theorem}
\begin{proof}[Idee]
  Berechne nun die Vielfachheit $n_1$ der trivialen Darstellung $\C$ in der
  adjungierten Darstellung $\C[G]_{\text{adj}}$ auf zwei Arten
  \begin{enumerate}[(i)]
    \item Mit Maschke und Schur gilt
      $\hom_\C(V_j,V_j)^G\equiv\hom_G(V_j,V_j)\cong\C$.  Also gilt
      $n_1=\dim(\C[G]^G)=m$, wobei $m$ die Anz. irred. Darstellungen von
      $G$ ist.
    \item Andererseits gilt für den Charakter der Darst. $\C[G]_{\text{adj}}$
      folgendes. $\chi(1)=|\stab(g)|$ und somit $n_1=\inner{\chi_1}{\chi}
      =n_{\text{conj}}$, wobei $n_{\text{conj}}$ die Anzahl der
      Konjugationsklassen von $G$ ist und wir verwendet haben, dass alle
      Konjugationsklassen gleich gross sind.
  \end{enumerate}
\end{proof}

\begin{corollary}
  Die Charaktere der irreduziblen Darstellung bilden eine ONB des Raumes der
  Klassenfunktionen $L^2(G)^G$. Das heisst die  Zeilen der Charaktertafel
  orthogonal bezüglich des \textbf{gewichteten Skalarprodukt} sind.
  \begin{equation*}
    \langle f, \tilde{f} \rangle = \frac{1}{|G|} \sum_{j = 1}^n |c_j|
    \bar{f(c_j)}\tilde{f}(c_j)
  \end{equation*}
  Wobei im Falle der Charaktertafel $c_j$ eine Konjugationsklasse ist und f,
  $\tilde{f}$ Charaktere zweier Darstellungen.
\end{corollary}
\begin{proof}[Idee]
  Die Charaktere sind orthonormal und in der Zahl gleich der Dimension von
  $L^2(G)^G$.
\end{proof}


\section{Charaktertafel}
\begin{theorem}\textbf{Zweite Orthogonalitätsrelation der Charaktere}\\
  Die Spalten der Charaktertafel sind orthogonal bzgl. dem Standardskalarprodukt
  \begin{equation*}
    \sum_{k=1}^n\overline{\chi_k(C_\alpha)}\chi_k(C_\beta)
    =\delta_{\alpha\beta}\frac{|G|}{|C_\alpha|}.
  \end{equation*}
  Eine analoge Aussage gilt auch für die Zeilen mit
  \begin{equation*}
    \sum_{k=1}^n|(C_k)|\overline{\chi_\alpha(C_k)}\chi_\beta(C_k)=\delta_{\alpha\beta}
    |G|.
  \end{equation*}
\end{theorem}

\begin{theorem}
  Seien $G,G'$ endliche Gruppen. Seien $C_1,\dots,C_m$, $\rho_1,\dots,\rho_m$ und
  $\chi_1,\dots,\chi_m$ die Konjugationsklassen, irreduzieblen Darstellungen und
  irreduziblen Charakter von G. Seien analog $C'_1,\dots,C'_n$,
  $\rho'_1,\dots,\rho'_n$ und $\chi'_1,\dots,\chi'_n$ diejenigen von $G'$. Dann
  gelten
  \begin{enumerate}[(i)]
    \item Die Konjugationsklassen von $G \times G'$ sind $C_i \times C_j'$ für
      $i \in \{ 1,\dots,m \}$ und $j \in \{ 1,\dots,n \}$.
    \item Die irreduziblen Darstellungen sind in der Tat $\rho_{ij} = \rho_i
      \otimes \rho_j'$, mit den Charakteren $\chi_{ij}(g,g') =
      \chi_i(g)\chi_j'(g')$, wobei $i \in \{ 1,\dots,m \}$ und $j \in \{
        1,\dots,n \}$.
  \end{enumerate}
\end{theorem}

\begin{remark}
  Sprich, wenn wir die Charaktertafel einer Gruppe $H$ aufstellen wollen,
  wobei $H=G \times G'$ wie oben, so findet sich diese durch "tensorieren" der Charaktertafeln von $G$ und $G'$.
\end{remark}