\chapter{Minkowskiraum und Lorentz-Gruppe}

\begin{definition}
	Der Minkowskiraum ist $\mathbb{R} ^4$ mit der symmetrischen Bilinearform
	\begin{equation*}
		(x, y) = x_0 y_0 - \sum_{i=1} ^3 x_i y_i = x^Tgy,
	\end{equation*}
	für $g=\operatorname{diag} (1,-1,-1,-1)$. Die Lorentzgruppe ist $O(1,3) = \{ A \in \GL(4, \mathbb{R}) \colon\,\forall x \in \mathbb{R}^4\colon ( Ax, Ax ) = ( x, x ) \}$. Wir nennen eine Basis $(b_i)_i$ \textbf{orthonormal}, falls  $\langle b_i, b_j \rangle = g_{ij}$. Wir definieren die folgenden Untergruppen von O(1,3):
	\begin{enumerate}[(i)]
		\item $O_+ (1,3) = \{A \in O(1,3) \vert A_{00} > 0\}$ (orthochrone Lorentz-trsf.)
		\item $SO(1,3) = \{A \in O(1,3) \vert \det A = 1\}$
		\item $SO_+(1,3) = \{A \in O(1,3) \vert A_{00} >  0 \wedge \det A = 1\}$
	\end{enumerate}
\end{definition}

\begin{remark}
Ein Zeitartiger Einheitsvektor $x\in\R^4$ hat immer die Form
\begin{equation*}
    x=\begin{pmatrix}\pm\cosh\alpha\\ \sinh\alpha \cdot n\end{pmatrix}, \quad n\in S^2, \alpha\in\R,
\end{equation*}
und eine $4\times 4$-Matrix ist in $O(1,3)$ genau dann, wenn die Zeilen bzw. Spalen orthonormal im obigen Sinne sind.
\end{remark}

\begin{definition}
    Wir definieren mit $\chi\in\R$ der Rapidität einen Lorentz-Boost in $3$-Richtung als
    \begin{tabular}{ll}
    $
        \begin{pmatrix}
            \cosh\chi & 0 & 0 & \sinh\chi\\ 
            0         & 1 & 0 & 0\\ 
            0         & 0 & 1 & 0\\ 
            \sinh\chi & 0 & 0 & \cosh\chi
        \end{pmatrix}
    $
    &
\begin{tabular}{l}
wobei $\tanh\chi=\frac{v}{c}$ für \\
$v$ die Geschwindigkeit in $3$-Richtung.
\end{tabular}
\end{tabular}
\end{definition}

\begin{proposition}
$SO_+(1,3)$ ist zusammenhängend und $O(1,3)$ zerfällt in vier zusammenhangskomponenten
\begin{equation*}
    O(1,3)=SO_+(1,3)\sqcup PSO_+(1,3)\sqcup TSO_+(1,3)\sqcup PTSO_+(1,3)\sqcup 
\end{equation*}
mit $P$ einer Raumspiegelung und $T$ einer Zeitumkehrung.
\end{proposition}

\begin{definition}\textbf{Homomorphismus $SL(2,\C)_\R\rightarrow SO_+(1,3)$}
Sei $H=\{\hat{x}=x_0\sigma_0+\mathbf{x}\cdot\sigma : x=(x_0,\mathbf{x})\in\R^4\}\cong \R^4$ mit $\det \hat{x} = (x,x)$ dem Minkowskiskalarprodukt.
	Wir definieren die $\underline{reelle}$ Darstellung
	\begin{equation*}
	\begin{aligned}
		\rho:\, &SL(2, \mathbb{C})_{\mathbb{R}} \rightarrow SO_+(1,3)\subseteq\GL(H) \\
		&\rho(A) X = A X A^\dagger.
	\end{aligned}
	\end{equation*}
	Der Homomorphismus auf der Lie-Algebra nimmt folgende Form an:
	\begin{equation*}
		\nu = D_\mathbb{1}\rho : \mathfrak{sl}(2, \mathbb{C})_{\mathbb{R}} \rightarrow \mathfrak{so}(1,3)	
\end{equation*}	
\end{definition}

\begin{theorem}
	$\rho$ ist surjektiv mit ker$\rho = \{\pm 1\}$. ($\Rightarrow SO_+(1,3) = SL(2, \mathbb{C}) / \{\pm 1 \})$. Wir können ebenfalls zeigen, dass $\nu$ ein Isomorphismus von $\R$-Lie Algebren ist. Daraus folgt $\mathfrak{sl}(2, \mathbb{C})_\mathbb{R} \cong \mathfrak{so}(1,3)$. \newline
\end{theorem}

\begin{remark}
Für $\mathfrak{g}$ eine komplexe Lie-Algebra gilt (über $\C$)
	\begin{equation*}
		\mathfrak{g}_\mathbb{R} \otimes \mathbb{C} \cong \mathfrak{g} \oplus \mathfrak{g},
	\end{equation*}
insbesondere gilt
$
    \sO (1,3)\otimes\C \cong \mathfrak{sl}(2, \mathbb{C})_\mathbb{R}\ \otimes\C \cong \mathfrak{sl}(2, \mathbb{C})_\mathbb{R}\ \oplus \mathfrak{sl}(2, \mathbb{C})_\mathbb{R}\ .
$
\end{remark}

\begin{remark}
Sei $J$ die Multiplikation mit i in $\mathfrak{g}$. Wir verwenden nun $J$ auf $\mathfrak{g}_\R$, wo $i$ durch Komplexifizierung hinzugefügt wurde. Dann ist $J$ $\C$-diagonalisierbar und $\mathfrak{g}_\R\otimes\C\cong\mathfrak{g}_+\oplus\mathfrak{g}_-$ zerfällt in zwei Eigenräume selber Dimension. Betrachte den Projektor
\begin{equation*}
    \Pi^\pm = \frac{1}{2}\left(1\mp iJ\right)
\end{equation*}
\end{remark}

\begin{lemma}
  \begin{enumerate}[(i)]
      \item $\mathfrak{g}_\pm$ sind Lie Unteralgebren und $[g_+,g_-]=0$, womit $\mathfrak{g}\cong\mathfrak{g}_+\oplus\mathfrak{g}_-$ als komplexe Lie Algebren gilt.
      \item $\mathfrak{g}^\pm$ sind isomorph zu $\mathfrak{g}$. Der Isomorphismus lautet
      \begin{equation*}
              \phi_\pm \colon \mathfrak{g} \rightarrow \mathfrak{g}_\pm \subseteq \mathfrak{g}_\R\otimes\C\\
              \qquad x \mapsto \Pi^\pm x
      \end{equation*}
      \item Die Abbildung reller Lie Algebren lautet
      \begin{equation*}
          \begin{aligned}
              \mathfrak{g}_\R &\rightarrow \mathfrak{g}_\R\otimes\C\cong\mathfrak{g}_+\oplus\mathfrak{g}_- &\xrightarrow{\cong} \mathfrak{g}\oplus\mathfrak{g}\\
              x &\mapsto x = \Pi^+x + \Pi^-x &\mapsto (x,\bar{x})
          \end{aligned}
      \end{equation*}
  \end{enumerate}
\end{lemma}

\begin{theorem}
	Seien $ \mathfrak{g}, \mathfrak{\tilde{g}}$ komplexe, halbeinfache Lie-Algebren. Dann sind die komplexen endl.dim. irred. Darstellungen gerade die Darstellungen
	\begin{equation*}
		\rho = \rho _\lambda \otimes \rho_{\tilde{\lambda}} : \mathfrak{g} \oplus \mathfrak{\tilde{g}} \rightarrow \mathfrak{gl}(V_\lambda \otimes V_{\tilde{\lambda}}).
	\end{equation*}
	Hierbei sind $\rho _\lambda : \mathfrak{g} \rightarrow \mathfrak{gl}(V_\lambda)$ und $\rho_{\tilde{\lambda}} : \mathfrak{\tilde{g}} \rightarrow \mathfrak{gl}(V_{\tilde{\lambda}})$ die irreduziblen Darstellungen von $\mathfrak{g}$ und  $\mathfrak{\tilde{g}}$. Also haben die irreduziblen Darstellungen von $\mathfrak{sl}(2, \mathbb{C}) \oplus \mathfrak{sl}(2, \mathbb{C})$ gerade die Form $V_n \otimes V_m$. Jene von $\mathfrak{so}(1,3) \cong \mathfrak{sl}(2, \mathbb{C})$ haben die Form $V_n \otimes \overline{V_m}$.
\end{theorem}