\chapter{Lie-Gruppen \& Lie-Algebren}
Fragestellung: Wie kommt man von der Darstellung der Lie-Algebra $\mathfrak{g}$ 
auf die Darstellung der Lie-Gruppe G?

\section{Exponentialabbildung von Matrizen}
\begin{definition}
  Wir definieren die Standard-Norm wie auch die Operatornorm auf dem Raum der Matrizen $\K^{n\times n}$, mit $\K\in\{\R,\C\}$
  \begin{equation*}
    \|x\|=\left(\sum_{i, j=1}^{n}\left|x_{i j}\right|^{2}\right)^{1 /
    2}=\left(\operatorname{tr}\left(X^{*} X\right)\right)^{1 / 2}
  \qquad
      \|X\|_{\text{op}}=\sup_{\|v\|_2=1}\|Xv\|_2,\quad X\in\K^{n\times n}
  \end{equation*}
  Beide Normen sind submultiplikativ, d.h. $\|XY\|\leq\|X\|\|Y\|$ für alle $X,Y\in\K^{n\times n}$.
\end{definition}

\begin{lemma}
  Folgende Reihe konvergiert absolut $\forall X \in \K^{n\times n}$, nach wie vor $\K\in\{\R,\C\}$.
  \begin{equation*}
  \exp (X)=\sum_{k=0}^{\infty} \frac{1}{k !} X^{k},
  \end{equation*}
\end{lemma}

\begin{lemma}
  Seien $X,Y \in \K^{n\times n}$ und $\K\in\{\R,\C\}$. Es gilt:
  \begin{enumerate}[(i)]
    \item $\exp (X) \exp (Y)=\exp (X+Y) =\exp (Y) \exp(X) \text {, falls } X Y=Y X$
    \item $\exp (X) \text { ist invertierbar}: \ \operatorname{exp}(X)^{-1}=\exp (-X)$
    \item $A \exp (X) A^{-1}=\exp \left(A X A^{-1}\right), \quad A \in G L(n,
      \mathbb{K})$
    \item  $\operatorname{det}(\exp (X))=\exp (\operatorname{tr}(X))$
    \item $\exp \left(X^{\dagger}\right)=(\exp (X))^{\dagger}$
    \item $X \mapsto \exp(X)$ ist analytisch und $\frac{\D}{\Dt}\exp(tX)=X\exp(tX)$ für $t\in\R$.
  \end{enumerate}
\end{lemma}

\begin{theorem} \textbf{Baker-Campbell-Hausdorff} \\
	Seien $X, Y$ stetige lineare Operatoren eines Banachraums. Sei zudem $[X, [X, Y]] = 0$ und $[Y, [Y, X]] = 0$. Dann gelten die Formeln:
	\begin{equation*}
	\begin{aligned}
		e^X e^Y &= e^Y e^X e^{[X,Y]} \\
		e^{X+Y} &= e^X e^Y e^{-\frac{[X,Y]}{2}} 
	\end{aligned}
	\end{equation*}
	Ebenfalls gilt die allgemeine Aussage: $\forall t \in \mathbb{R}$:
	\begin{equation*}
		\operatorname{exp}(tX) Y \operatorname{exp(-tX)} = Y + t[X, Y] + \frac{t^2}{2!}[X, [X, Y]] + \frac{t^3}{3!} [X, [X, [X, Y]]] + \ldots .
	\end{equation*}
\end{theorem}



\begin{lemma}
  Die Abbildung $\exp \colon \K^{n\times n}\rightarrow \GL(n,\K)$ ist in einer Umgebung U von $\mathbb{1}$ invertierbar. Die inverse Abbildung ist explizit durch die folgende konvergente Reihe gegeben
  \begin{equation*}
    \operatorname{log}(X) = \sum_{n=1}^{\infty}(-1)^{n+1} \frac{(X-\mathbb{1})^{n}}{n} \qquad \text{ falls } \|X-\mathbb{1}\|_{\text{op}}<1.
  \end{equation*}
\end{lemma}

\begin{corollary}
  Wegen dem Inversen Funktions Theorem ist exp lokal invertierbar. Insbesondere
  enthält $\operatorname{exp}(\mathfrak{g}) \coloneqq
  \operatorname{Im}(\operatorname{exp})$ eine offene Umgebung von $1 \in G$.
\end{corollary}

\section{Allgemeine Exponentialabbildung}
\begin{definition}
  Sei $G$ eine Lie-Gruppe. Wir definieren die \textbf{Linksmultiplikationsabbildung} $L_g$
  für ein $g \in G$ mit
  \begin{equation*}
    \begin{aligned}
      L_g : G &\rightarrow G \\
      h \in G &\mapsto L_g h = gh
    \end{aligned}
  \end{equation*}
\end{definition}

\begin{definition}
  Sei $G$ eine Lie-Gruppe und $\mathfrak{g}$ die dazugehörige Lie-Algebra. Wir definieren ein $C^\infty$-Vektorfeld $v_x$ auf $G$ zu jedem $x \in \mathfrak{g}$ wie folgt
  \begin{equation*}
    v_x : g\in G \longmapsto v_x(g) = (D_\mathbb{1}L_g)(x) \in T_g G.
  \end{equation*}
\end{definition}

\begin{theorem}
  Die Differentialgleichung
  \begin{equation*}
      \dot{\varphi}_x(t) = v_x(\varphi_x(t)), \qquad
      \varphi_x (0) = \mathbb{1} \in G.
    \end{equation*}
  besitzt eine eindeutige und maximale Lösung  $\varphi_x$.
  Für diese gilt das Additionsgesetz $\varphi_x(t+s)=\varphi_x(t)\varphi(s)$ für alle $t,s$ im Definitionsbereich von $\varphi_x$, weshalb $\varphi_x$ sogar global auf $\R$ definiert ist. 
\end{theorem}

\begin{remark}
  Die Menge der $\varphi_x$ wird auch \textbf{einparametrige
  Untergruppe} genannt. Ausserdem ist $\varphi_x: \mathbb{R}\longrightarrow G$
  ein Gruppenhomomorphismus.
\end{remark}

\begin{definition}
  Die \textbf{Exponentialabbilung} $\exp: \mathfrak{g} \rightarrow G$ ist
  definiert als
  \begin{equation*}
    \mathfrak{g} \ni x \longmapsto \operatorname{exp}(x) \coloneqq \varphi_x(1)
  \end{equation*}
\end{definition}

\begin{remark}
\begin{enumerate}[(i)]
    \item Die Exponentialabbildung ist glatt und es gilt $D_0\exp = \mathbb{1}$. Also ist $\exp$ lokal invertierbar und $\exp(\mathfrak{g})$ enthält eine offene Umgebung der $\mathbb{1}\in G$.
    \item Das Bild $\exp(\mathfrak{g})$ liegt in der Zusammenhangskomponente von $\mathbb{1}\in G$. Aber $\exp$ ist nicht immer surjektiv, auch nicht, falls $G$ zusammenhängend ist.
    \item Ist $G$ kompakt (oder nilpotent) \ul{und} zusammenhängend, so ist $\exp$ surjektiv. Insbesondere gilt das für $\exp: \mathfrak{gl}(V) \rightarrow GL(V)$.
    \item $\exp$ ist verträglich mit Lie Gruppen Homomorphismen $F\colon G\rightarrow H$. Für $f=D_\mathbb{1}F\colon T_\mathbb{1}G\rightarrow T_\mathbb{1}H$ und alle $x\in T_\mathbb{1}G$ gilt 
\begin{equation*}
    F(\underbrace{\operatorname{exp}_G(x)}_{\in G}) =
    \operatorname{exp}_H(\underbrace{f(x)}_{\in \mathfrak{h}})
\end{equation*}
\end{enumerate}
\end{remark}

\begin{definition}
  Eine Menge $G\subseteq\R^{n}$ ist zusammenhängend genau dann wenn für jede Teilmenge $V\subseteq G$, die sowohl relativ abgeschlossen als auch relativ offen ist, gilt $V=\emptyset$ oder $V=G$.
\end{definition}

\begin{theorem}
  Sei $G$ eine zusammenhängende Lie-Gruppe. Dann erzeugt jede offene Umgebung U von $\mathbb{1} \in G$ die Gruppe G. Das heisst, dass $\forall g \in G: g=g_1g_2\cdots g_n$ mit $n \in \mathbb{N}$ und $g_1,\ldots,g_n \in U$.
\end{theorem}

\begin{corollary}
  Seien $G$ und $H$ Lie-Gruppen und sei $G$ zusammenhängend. Dann ist die Abbildung
  \begin{equation*}
    \begin{aligned}
      \operatorname{Hom}_{\text{Lie-Gruppe}}(G,H) &\longrightarrow
      \operatorname{Hom}_{\text{Lie-Algebra}}(\mathfrak{g},\mathfrak{h})\\
      F &\longmapsto f = D_\mathbb{1}F
    \end{aligned}
  \end{equation*}
  injektiv; der Lie Gruppen Homomorphismus ist also eindeutig durch den dazuehörigen Lie Algebren Homomorphismus definiert.
\end{corollary}

\begin{remark}
  Das heisst, dass die Lie-Gruppenhomomorphismen eindeutig bestimmt sind durch
  die zugehörigen Lie-Algebrahomomorphismen.
  Der Spezialfall H=GL(V) besagt, dass jede Darstellung von G eindeutig bestimmt
  ist durch eine Darstellung von $\mathfrak{g}$.
\end{remark}

\begin{theorem}
  Ist G zusätzlich einfach zusammenhängend, so ist die obige Abbildung sogar
  bijektiv.
\end{theorem}

\begin{example}
$\exp(\mathfrak{sl}(n,\mathbb{R}) \neq SL(n,\mathbb{R})$. Wir wissen aber, dass $\exp{\mathfrak{sl}(n,\mathbb{C}} \subset \{ X \in \mathbb{R}^{n \times n} | \operatorname{tr}(X)> -2 \} \cup \{-\mathbb{1} \}$.
\end{example}

\begin{remark}
Sind zwei Lie-Algebren isomorph zueinander so müssen die dazugehörigen Lie-Gruppen \ul{nicht} isomorph sein. Umgekehrt müssen für isomorphe Lie-Gruppen aber die dazugehörigen Lie-Algebren isomorph sein. Ist die Lie-Gruppe einfach-zusammenhängend gilt dies aber in beide Richtungen. 
\end{remark}