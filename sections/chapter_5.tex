\chapter{Halb-einfachen Lie-Algebren}
In diesem Kapitel ist $\mathfrak{g}$ immer eine komplexe, endlich-dimensionale, halb-einfache Lie Algebra, ausser dies ist ausdrücklich anders vermerkt.

\section{Struktur von halb-einfachen Lie Algebren}

\section{Definitionen}
\begin{definition}
  Sei $\mathfrak{g}$ eine komplexe halb-einfache Lie-Algebra. Eine Unteralgebra
  $\mathfrak{h} \subset \mathfrak{g}$ heisst \textbf{torisch}, falls sie
  folgendes erfüllt:
  \begin{enumerate}
    \item Sie ist abelsch.
    \item $\forall x \in \mathfrak{h}: \ \operatorname{ad}_x$ ist diagonalisierbar.
  \end{enumerate}
  Eine torische Unteralgebra wird \textbf{maximal} oder \textbf{Cartan} genannt,
  falls sie Maximal ist unter allen torischen Unteralgebren. ($\mathfrak{g} \supseteq \mathfrak{h}' \supsetneq \mathfrak{h} \Rightarrow \mathfrak{h}'$ ist nicht torisch)
\end{definition}

\begin{theorem}
  Seien $\mathfrak{h}_1,\mathfrak{h}_2 \subset \mathfrak{g}$ zwei
  Cartan-Unteralgebren der halb-einfachen Lie-Algebra $\mathfrak{g}$. Dann existiert ein Automorphismus $\phi$ auf $\mathfrak{g}$, so dass gilt $\phi(\mathfrak{h}_1) = \mathfrak{h}_2$. Insbesondere haben dadurch alle Cartan-Unteralgebren dieselbe Dimension, welche man den \textbf{Rang der Lie Algebra} nennt.
\end{theorem}

\begin{remark}\label{rem:zerlegung_he_la}
  Eine Familie kommutierender diagonalisierbarer linearer Operatoren auf einen endlich-dimensionalen Vektorraum ist simultan diagonalisierbar (bekanntes Resultat). Also sind für alle $x\in\mathfrak{h}$ die Operatoren $\operatorname{ad}_x$ simultan diagonalisierbar. Die Eigenwerte (genannt \textbf{Wurzeln}) $\alpha\neq 0$ bilden das \textbf{Wurzelsystem} $\Delta$, so dass für $\alpha\in\Delta\subseteq\mathfrak{h}^*$
  \begin{equation*}
      \operatorname{ad}_x(y)=\alpha(x)y,\quad y\in\mathfrak{g}_\alpha=
    \{v \in V | \rho(h) v=\alpha(h) v \, \forall h \in \mathfrak{h}\}
,\quad x\in\mathfrak{h}
  \end{equation*}
  und $\mathfrak{g}_\alpha$ der simultane Eigenraum zum Eigenwert $\alpha$ ist. 
  Anders: "$\operatorname{ad}_x$ wirkt wie $\alpha (x)$ auf $\mathfrak{g}_\alpha$."
Schliesslich zerlegen wir die halb-einfache Lie Algebra in die direkte Summe der Unterräume,
  \begin{equation*}
      \mathfrak{g}=\mathfrak{h}\oplus\left(\bigoplus_{\alpha\in\Delta}\mathfrak{g}_\alpha\right),
  \end{equation*}
  wobei $\mathfrak{h}$ der Eigenraum zum Eigenwert $0$ ist, weil $\mathfrak{h}$ abelsch ist. Diese Zerlegung wird \textbf{Gewichtsraum-Zerlegung} genannt.
\end{remark}

\begin{proposition}\label{prop:gcp}x
  Sei $\mathfrak{h}\subseteq\mathfrak{g}$ eine Cartan-Unteralgebra der h.e. Lie Algebra $\mathfrak{g}$. Dann gilt für $\mathfrak{g}_\alpha$ wie in Bemerkung 
\ref{rem:zerlegung_he_la} und für $\mathfrak{g}_0 = \mathfrak{h}$ folgendes
  \begin{enumerate}[(i)]
    \item $\left[ \mathfrak{g}_\alpha , \mathfrak{g}_\beta \right] \subset
    \mathfrak{g}_{\alpha + \beta}$.
    \item Falls $\alpha,\beta \in \{ 0 \} \cup \Delta$ und $\alpha + \beta \neq
      0$, dann ist die Killungform $K(\mathfrak{g}_\alpha ,
      \mathfrak{g}_{\beta}) = 0$.
    \item Die Killing-Form $K$ ist auf $\mathfrak{g}_{\alpha}\times\mathfrak{g}_{-\alpha}$ nicht degeneriert für alle $\alpha\in\{0\}\cup\Delta$, insbesondere auf $\mathfrak{h}\times\mathfrak{h}$.
    \begin{equation*}\label{eq:dual_root}
        \Rightarrow \forall\alpha\in\Delta\,\exists H_{\alpha}\in\mathfrak{h}\colon \alpha(x)=K(H_{\alpha},x)\quad (\forall x\in\mathfrak{h}).
    \end{equation*}
    \item $\alpha \in \Delta \Rightarrow -\alpha \in \Delta$.
    \item Die Killing-Form auf $\mathfrak{h} \times \mathfrak{h}$ ist
      $K(h,h^\prime) = \sum_{\alpha \in \Delta} \alpha (h) \alpha (h^\prime)$.
    \item $\operatorname{span}_\C\Delta=\mathfrak{h}^*$.
  \end{enumerate}
\end{proposition}

\begin{lemma}
  Wähle $0 \neq E_\alpha \in \mathfrak{g}_\alpha$ und $E_{-\alpha} \in \mathfrak{g}_{-\alpha}$, so dass $K(E_\alpha , E_{-\alpha}) = 1$. Dann gilt $[E_{\alpha},E_{-\alpha}]=H_{\alpha}\in\mathfrak{h}$, wobei $H_{\alpha}$ das zu $\alpha$ duale Element bezüglich der Killing-Form ist aus Proposition \ref{prop:gcp}, Glg. \eqref{eq:dual_root}. Es ist $\alpha(H_\alpha)\neq 0$ und
  \begin{equation*}
    \begin{matrix}
        h=\frac{2}{\alpha(H_\alpha)}H_\alpha, & e=\frac{2}{\alpha(H_\alpha)}E_\alpha, & f=E_{-\alpha}
    \end{matrix}
  \end{equation*}
  spannen eine Lie Unteralgebra von $\mathfrak{g}$ auf, die isomorph zu $\sL[2]\ $ ist.
\end{lemma}

\begin{remark}
Dies bedeutet, dass "in halb-einfachen Lie-Algebren gibt es immer viele
Kopien von $\mathfrak{sl}(2,\mathbb{C})$."
Denn die Relationen von $\mathfrak{sl}(2,\mathbb{C})$ sind erfüllt:
\begin{equation*}
  \begin{aligned}
    [h,e] &= \frac{2}{\alpha (H_\alpha )} \alpha (H_\alpha ) e = 2e \\
    [h,f] &= \frac{2}{\alpha (H_\alpha)} (-\alpha (H_\alpha )f) = -2f \\
    [e,f] &= \frac{2}{\alpha (H_\alpha)} [E_\alpha , E_{-\alpha} ] = \frac{2
      H_\alpha}{\alpha (H_\alpha )} = h
  \end{aligned}
\end{equation*}
Betrachten wir das Lemma in unserem Beispiel, so können wir wegen (iii) in Proposition. 4.7 verlangen, dass gelten soll
$\lambda_{ij} (x) = x_i - x_j \rightsquigarrow \Delta = \{ \lambda_{ij} | 1 \leq
i < j \leq n \} $ und beachte, dass $\lambda_{ji} = - \lambda_{ij}
\Longrightarrow \left( \alpha \in \Delta \Longleftrightarrow -\alpha \in \Delta
\right)$. Wir berechnen nun

\begin{equation*}
  K(E_{ij},E_{kl}) = 2n \operatorname{tr}(E_{ij}E_{kl}) = 2n
    \delta_{jk}\delta_{il} = 0 \quad \text{für } (i,j) \neq (k,l)
\end{equation*}
und wegen (ii) von Proposition. 4.7 ist dies äquivalent zu
$\lambda_{ij} = \lambda_{l,k} = - \lambda_{kl}$.\\ In unserem Beispiel ($\mathfrak{sl}(n,\mathbb{C})$) werden die
Kopien von $\mathfrak{sl}(2,\mathbb{C})$
aufgespannt durch
\begin{equation*}
  \underbrace{E_{ij}}_{= E_{\lambda_{ij}}} , \underbrace{E_{ji}}_{= 2n
  E_{\lambda_{ji}}} , \underbrace{E_{ii}-E_{jj}}_{H_{\lambda_{ij}}}
\end{equation*}
Wir wollen nun eine \textbf{nicht-degenerierte bilinearform} $\langle , \rangle$
auf $\mathfrak{h}^*$ definieren, in dem wir die Killing-Form dualisieren.
Insbesondere definieren wir
\begin{equation*}
  \langle \alpha , \beta \rangle \coloneqq K(H_\alpha , H_\beta )
\end{equation*}
\end{remark}

\begin{example}
  Sei $\mathfrak{g} = \mathfrak{sl}(n,\mathbb{C})$. Dann sind die spurlosen
  Diagonalmatrizen eine abelsche Unteralgebra $\mathfrak{h}$ für welche
  $\operatorname{ad}_x$ offensichtlich diagonalisierbar ist. $\mathfrak{h}$ ist
  maximal, da jede Matrix, welche mit allen spurlosen Diagonalmatrizen
  kommutiert, wiederum diagonal sein muss. Daher ist $\mathfrak{h}$ Cartan und
  der Rang von $\mathfrak{sl}(n,\mathbb{C})$ ist n-1. Wir können also die
  Zerlegung
  \begin{equation*}
    \mathfrak{g} \cong \mathfrak{h} \ \oplus \underset{\alpha \in \Delta}{\bigoplus} \
      \mathfrak{g}_\alpha
  \end{equation*}
  erhalten, wobei $\Delta \subset \mathfrak{h}^*$ die endliche Menge der
  nicht-trivialen Eigenwerte ist.
\end{example}


\begin{definition}
  Definiere auf $\mathfrak{h}^*$ eine Bilinearform $\inner{\cdot}{\cdot}$ dual zur Killingform, sodass für alle $\alpha,\beta\in\Delta$ gilt
  \begin{equation*}
      \inner{\alpha}{\beta}=K(H_{\alpha},H_{\beta}),
  \end{equation*}
  wobei $H_{\alpha}$ und $H_{\beta}$ wie in Glg. \eqref{eq:dual_root} definiert sind. 
\end{definition}

\begin{theorem}
  Seien $\mathfrak{h}_0=\underset{\alpha\in\Delta}{\operatorname{span}}\,_\R H_\alpha\subseteq\mathfrak{h}$ und $\mathfrak{h}_0^*=\underset{\alpha\in\Delta}{\operatorname{span}}\,_\R \alpha\subseteq\mathfrak{h}^*$ zwei relle Unterräume. Dann sind
  \begin{equation*}
      \mathfrak{h}_0\otimes\C\cong\mathfrak{h},\quad \mathfrak{h}_0^*\otimes\C\cong\mathfrak{h}^*.
  \end{equation*}
  Insbesondere ist $\dim_\R\mathfrak{h}_0=\dim_\R\mathfrak{h}_0^*=\frac{1}{2}\dim_\R\mathfrak{h}=\frac{1}{2}\dim_\R\mathfrak{h}^*\equiv r(=\dim_\C\mathfrak{h})$ der Rang der Lie Algebra. Weiter sind $K(\cdot,\cdot)$ und $\inner{\cdot}{\cdot}$ positiv definit auf $\mathfrak{h}_0$ bzw. $\mathfrak{h}_0^*$, also ein Skalarprodukt.
\end{theorem}

\begin{definition}
  Sei $\alpha \in \Delta , \beta \in \Delta \cup \{ 0 \}$. Der \textbf{$\alpha$-string durch $\beta$} ist definiert als die Untermenge
  \begin{equation*}
    \underset{n\in \mathbb{Z}}{\bigoplus} \mathfrak{g}_{n\alpha + \beta}
  \end{equation*}
\end{definition}

\begin{proposition}
  Ein $\alpha$-string durch $\beta$ hat nicht-triviale Terme für $-p \leq n \leq
  q$ mit $p,q \geq 0$ ohne Lücken. Ausserdem gilt
  \begin{equation*}
    p-q = \frac{2\langle \alpha , \beta \rangle}{\langle \alpha , \alpha
      \rangle} \in \mathbb{Z}
  \end{equation*}
\end{proposition}

\begin{remark}
  Es gilt sogar für alle $\alpha,\beta\in\Delta$, dass $2\frac{\inner{\alpha}{\beta}}{\inner{\alpha}{\alpha}}\in\pm\{0,1,2,3\}$.
\end{remark}

\begin{corollary}
  $\mathfrak{h}_0$ ist die reelle Form von $\mathfrak{h}$, d.h. die kanonische Abbildung $\mathfrak{h}_0\otimes\C\rightarrow\mathfrak{h}$ ist ein Isomorphismus von Vektorräumen.
\end{corollary}

\begin{corollary}
  Sei $\mathfrak{h}_0$ eine reale Form für $\mathfrak{h}$. Der reale
  Span $\mathbb{R}\Delta$ von $\Delta$ ist eine reale Form von
  $\mathfrak{h}^*$, kannonisch verbunden mit $\mathfrak{h}_0^*$. Die oben
  definierte Biliniearform $\langle , \rangle$ ist positiv Definit
  auf $\mathbb{R}\Delta \cong \mathfrak{h}_0^*$. Die Killing-Form
  ist positiv Definit auf $\mathfrak{h}_0$. \\
  Das Korollar in anderen Worten:\\
  Sei $\mathfrak{h}_0 = \underset{\alpha \in
  \Delta}{\mathbb{R}\operatorname{-span}(H_\alpha )} \subset \mathfrak{h}$ und
  $\mathfrak{h}_0^* = \underset{\alpha \in
  \Delta}{\mathbb{R}\operatorname{-span}(\alpha)} \subset \mathfrak{h}^*$. Dann
  gilt:\\
  $\mathfrak{h}_0 \bigotimes \mathbb{C} = \mathfrak{h}$ und $\mathfrak{h}_0^*
  \bigotimes \mathbb{C} = \mathfrak{h}^*$, also insbesondere
  $\operatorname{dim}_{\mathbb{R}} \mathfrak{h}_0 =
  \operatorname{dim}_{\mathbb{R}} \mathfrak{h}_0^* = \frac{1}{2}
  \operatorname{dim}_{\mathbb{R}} \mathfrak{h} = r$.\\
  Ausserdem ist $K(-,-)$ positiv Definit (also ein Skalarprodukt) auf
  $\mathfrak{h}_0$ und $\langle - , -\rangle$ ein Skalaprodukt auf
  $\mathfrak{h}_0^*$.
\end{corollary}
  Wir wollen nun eigentlich zwischen positiven und negativen Wurzeln
  unterscheiden können. Diese Unterscheidung ist, da es sich um Vektoren
  handelt, leider \textbf{nicht eindeutig}. Die folgende Definition ist daher zu
  einem gewissen Grad beliebig.

\begin{remark}[Totale Ordnung auf $\mathfrak{h}_0^*$]
  Gegeben Wurzeln $\alpha_1,\dots,\alpha_r\in\Delta\subseteq\mathfrak{h}_0^*$, sodass $H_{\alpha_1},\dots,H_{\alpha_r}$ eine Basis von $\mathfrak{h}_0$ bilden. Wir sagen, $0\neq\lambda\in\mathfrak{h}_0^*$ sei eine \textbf{positive Wurzel}, $\lambda > 0$, falls die erste nicht-verschwindende Zahl der Liste $\lambda(H_{\alpha_1}),\lambda(H_{\alpha_2}),\dots$ positiv ist. Wir sagen, $0\neq\lambda$ ist eine negative Wurzel, wenn $\lambda$ nicht positiv ist. Des Weiteren schreiben wir $\lambda >\lambda '$, genau dann wenn $\lambda -\lambda ' > 0$ und $\lambda\geq\lambda '$, genau dann wenn $\lambda >\lambda '$ oder $\lambda=\lambda '$. Dies definiert eine totale Ordnung auf $\mathfrak{h}_0^*$.
\end{remark}

\begin{definition}
  Eine Wurzel $\alpha \in \Delta$ ist \textbf{einfach}, falls $\alpha > 0$ und $\alpha$ nicht als die Summe von zwei anderen positiven Wurzeln geschrieben werden kann. Die Menge der einfachen Wurzeln wird mit $\Pi$ notiert.
\end{definition}

\begin{theorem}
  Die einfachen Wurzeln bilden eine Basis von $\mathfrak{h}_0^*$. Das bedeutet insbesondere:
  \begin{enumerate}[(i)]
      \item $\#\Pi = r$
      \item Jede positive Wurzel $\alpha$ kann als Linearkombination $\alpha = \sum_{j=1}^r n_j \alpha_j$ geschrieben werden. Dabei sind $\{\alpha_1,...,\alpha_r\}=\Pi$ die einfachen Wurzeln und $n_1,...,n_r$ natürliche  Zahlen.
  \end{enumerate}
\end{theorem}

\begin{definition}
  Das \textbf{Dynkin-Diagramm} der halb-einfachen Lie Algebra $\mathfrak{g}$ ist folgender Graph
  \begin{enumerate}[(i)]
      \item Knoten sind die einfachen Wurzeln $\alpha\in\Pi$, $r$ in ihrer Anzahl.
      \item Für $\alpha,\beta\in\Pi$ mit $\alpha\neq\beta$ verbinden wir die dazugehörigen Knoten mit folgender Anzahl an Kanten
      \begin{equation*}
          4\frac{\inner{\alpha}{\beta}^2}{\inner{\alpha}{\alpha}\inner{\beta}{\beta}}=4\cos^2(\measuredangle\alpha\beta).
      \end{equation*}
      \item Falls $\inner{\alpha}{\alpha}>\inner{\beta}{\beta}$, so zeichnen wir einen Pfeil auf die Kante von $\alpha$ nach $\beta$.
  \end{enumerate}
\end{definition}

\begin{theorem}
  Zwei halb-einfache Lie Algebren sind isomorph zueinander, genau dann, wenn ihre Dynkin-Diagramme gleich (bzw. isomorph) sind.
\end{theorem}

\subsection{Cartan-Unteralgebra von $\mathfrak{sl}(n,\C)$}\label{sec:cartan_sl2c}
Wir wählen für $\mathfrak{g}=\mathfrak{sl}(n,\C)$ den Raum der spurlosen Diagonalmatrizen als Cartan Unteralgebra
\begin{equation*}
    \mathfrak{h}=\left\{x=\begin{bmatrix}
        x_1 & \hdots  & 0\\ 
        \vdots & \ddots & \vdots\\ 
        0 & \hdots & x_n
    \end{bmatrix}\colon (x_i)_i\subseteq\C \text{ und } \sum_{i=1}^nx_i=0\right\}.
\end{equation*}
Wie definieren die Matrizen $E_{ij}\in\C^{n\times n}$ als die Matrizen mit einem $1$-Eintrag an der $ij$-ten Stelle und sonst nur Nullen. Für ein $x\in\mathfrak{h}$ gilt dann $\operatorname{ad}_xE_{ij}=(x_i-x_j)E_{ij}$, für $i\neq j$. Weiter ist $\rank\,\sL2\ = n-1 = \dim\mathfrak{h}$.
\begin{remark}
  Die Killing-Form auf $\mathfrak{sl}(n,\C)$ ist gegeben durch $K(x,y)=2n\tr(xy)$. Wir identifizieren den Raum $\mathfrak{h}^*$ als
  \begin{equation*}
      \mathfrak{h}^*\cong \C^n/\C\mathbb{I}\cong\left\{\begin{bmatrix}\lambda_1\\\vdots\\ \lambda_n\end{bmatrix}\colon (\lambda_i)_i\subseteq\C\text{ und } \sum_{i=1}^n\lambda_i=0 \right\}\cong\C^{n-1},
  \end{equation*}
  wo $\mathbb{I}=[1,\dots,1]$. Für $\lambda\in\mathfrak{h}^*$ gemäss der zweitletzten Identifikation gilt (per definitionem)
  \begin{equation*}
      \lambda(x)=\sum_{i=1}^n\lambda_i x_i, \quad x\in\mathfrak{h}.
  \end{equation*}
  Die duale Bilinearform $\inner{\cdot}{\cdot}$ nimmt auf $\mathfrak{h}^*$ die Form $\inner{\lambda}{\lambda'}=\frac{1}{2n}\sum_{i=1}^n\lambda_i\lambda'_i$ an.
\end{remark}
\begin{remark}
    Das Wurzelsystem ist $\Delta=\{\alpha_{ij}\colon1\leq i\neq j \leq n\}$, wobei $\alpha_{ij}(x)=x_i-x_j$ ist für $x\in\mathfrak{h}$. Für die Ordnungsrelation auf $\mathfrak{h}_0^*$ wählen wir die Basis $\{\alpha_{1n},\alpha_{2n},\dots,\alpha_{nn}\}$, bzw $\{H_{\alpha_{1n}},H_{\alpha_{2n}},\dots,H_{\alpha_{nn}}\}$ für $\mathfrak{h}_0$, wobei
  \begin{equation*}
      H_{\alpha_{in}}=\frac{1}{2n}(E_{ii}-E_{nn}).
  \end{equation*}
  Hiermit gilt dann $\alpha_{ij}>0\Leftrightarrow i>j$. Die einfachen Wurzeln sind gegeben durch $\Pi=\{\alpha_{i,i+1}\colon 1\leq i\leq n-1\}$. 
\end{remark}

\subsection{Cartan-Unteralgebra von $\mathfrak{so}(n,\C)$}
Für $\mathfrak{g}=\mathfrak{so}(n,\C)$ finden wir die folgenden beiden Cartan Unteralgebren
\begin{tabular}{ll}
$
    \mathfrak{h}=   \begin{bmatrix}
                    \begin{bmatrix} 0 & -x_1\\ x_1 & 0 \end{bmatrix} & 0 & \hdots & 0\\ 
                    0 & \begin{bmatrix} 0 & -x_2 \\ x_2 & 0 \end{bmatrix} &  & 0\\ 
                    \vdots &  & \ddots & \vdots\\ 
                    0 & 0 & \hdots & \begin{bmatrix} 0 & -x_1 \\ x_1 & 0 \end{bmatrix}
                    \end{bmatrix}$ & $\text{ falls } n=2k$
\\
$    \mathfrak{h}=   \begin{bmatrix}
                    \begin{bmatrix} 0 & -x_1\\ x_1 & 0 \end{bmatrix} & 0 & \hdots & 0 & 0\\ 
                    0 & \begin{bmatrix} 0 & -x_2 \\ x_2 & 0 \end{bmatrix} &  & 0 & 0\\ 
                    \vdots &  & \ddots & \vdots & \vdots\\ 
                    0 & 0 & \hdots & \begin{bmatrix} 0 & -x_1 \\ x_1 & 0 \end{bmatrix} & 0 \\
                    0 & 0 & \hdots & 0 & 0
                    \end{bmatrix}$ & $\text{ falls } n=2k+1$
\end{tabular}

In beiden Fällen stellt sich herraus, dass $\rank\,\mathfrak{so}(n,\C)=k$ ist. Wir verwenden fortan folgende Notation: Sei $X\in\C^{2\times 2}$, definiere für $n\geq 2$ die Matrix $A=[X]_{[i,j]}\in\C^{n\times n}$ als diejenige Matrix, welche im $(i,j)$-ten $2\times 2$-Block die Matrix $X$ hat und sonst nur Nullen. Es gilt $i,j\in\{1,2,\dots,\lfloor n/2\rfloor\}$. Dann bilden die Matrizen
\begin{equation*}
    H_j=\begin{bmatrix}
    0 & i\\ 
    -i & 0
    \end{bmatrix}_{[j,j]}
\end{equation*}
eine Basis von $\mathfrak{h}$. Die simultanen Eigenvektoren sind gegeben durch die Matrizen $E_{ij}^{\pm\pm}=[X_{\pm\pm}]_{[j,i]}-[X_{\pm\pm}^T]_{[i,j]}$, wobei \\
  \begin{tabular}{llll}
        $X_{++} =\begin{bmatrix} 1 & -i \\ -i & -1 \end{bmatrix}$ & $X_{+-}=\begin{bmatrix} 1 & i \\ -i & 1 \end{bmatrix}$ & 
        $X_{--} =\begin{bmatrix} 1 & i \\ i & -1 \end{bmatrix}$ & $X_{-+} =\begin{bmatrix} 1 & -i \\ i & 1 \end{bmatrix}$
\end{tabular}
Es gilt dann $\operatorname{ad}_{H_l}(E_{ij}^{\pm\pm})=(\pm\delta_{li}\pm\delta_{lj})E_{ij}^{\pm\pm})$. Falls $n=2k+1$ ungerade ist, so brauchen wir weiter die Matrizen $E_{i}^\pm=[X_\pm]_{[i,k+1]}-[X_\pm^T]_{[k+1,i]}$. In diesem Fall ist der Block $X_\pm$ ein Spaltenvektor der Form
\begin{equation*}
    \begin{aligned}
        X_{+}&=\begin{bmatrix} 1 \\ -i \end{bmatrix},\quad X_{-}&=\begin{bmatrix} 1 \\ i \end{bmatrix}.
    \end{aligned}
\end{equation*}
Weiter gilt $\operatorname{ad}_{H_l}(E_{i}^{\pm})=\pm\delta_{il}E_{i}^\pm$. Die Killingform auf $\mathfrak{h}$ nimmt für die Basiselemente $H_i$ die Form
\begin{equation*}
    K(H_i,H_j)=2(n-1)\delta_{ij}
\end{equation*}
an. Insbesondere ist $\mathfrak{h}_0=\operatorname{span}\R\{H_1,\dots,H_k\}$.

\section{Darstellungstheorie von halb-einfachen Lie Algebren}
\begin{lemma}\label{lem:diag_h}
  Sei $\rho$ eine endlich-dimensionale Darstellung einer komplexen halb-einfachen Lie-Algebra $\mathfrak{g}$. Fixiere eine Cartan-Unteralgebra $\mathfrak{h} \subset \mathfrak{g}$ und definiere die Elemente $H_\alpha$ wie oberhalb. Dann ist jedes $\rho (H_\alpha)$ diagonalisierbar. Die Eigenwerte sind ganzzahlige Vielfache von $\frac{\langle \alpha, \alpha \rangle}{2}$.
\end{lemma}

\begin{remark}[Beweis Strategie]
  Für die meisten Beweise bezüglich halb-einfachen Lie-Algebren empfiehlt es sich, die Einschränkung der Darstellung von $\mathfrak{g}$ auf die Darstellung der Unteralgebra $\mathfrak{s}_\alpha=\operatorname{span}\{ H_\alpha ,E_\alpha ,E_{-\alpha} \} \cong \mathfrak{sl}(2,\mathbb{C})$ zu betrachten oder die adjungierte Darstellung von $\mathfrak{s}_\alpha$ auf $\mathfrak{g}$.
\end{remark}

\begin{remark}
  Nach diesem Lemma können wir alle $\rho (h), h\in \mathfrak{h}$ \textbf{simultan
  diagonalisieren}.
\end{remark}

\begin{definition}
    Die simultane Eigenraumzerlegung der Darstellung $\rho$ von $\mathfrak{g}$
    \begin{equation*}
      V \cong \bigoplus_{\lambda \in W_{\rho}} V_{\lambda}
    \end{equation*}
    wird \textbf{Gewichtsraum-Zerlegung} genannt, wobei für die gemeinsamen Eigenwerte $\lambda \in W_{\rho}\subseteq \mathfrak{h}^*$
    \begin{equation*}
      V_{\lambda}=\{v \in V | \rho(h) v=\lambda(h) v \forall h \in \mathfrak{h}\}
    \end{equation*}
  die \textbf{Gewichtsräume} sind und die endliche Untermenge $W_V \subset \mathfrak{h}^*$ die Menge der \textbf{Gewichte} (Eigenwerte) ist.
\end{definition}

\begin{remark}
Die Tensorierung von zwei Darstellungen erhält die Gewichtsraumzerlegung, das heisst für $V=\bigoplus_{\lambda \in W} V_{\lambda} \quad \quad V^{\prime}=\bigoplus_{\lambda \in W^{\prime}} V_{\lambda}^{\prime}$ gilt
\begin{equation*}
V \otimes V^{\prime}=\bigoplus_{\lambda \in W+W^{\prime}}\left(\bigoplus_{\mu+\nu=\lambda} V_{\mu} \otimes V_{\nu}^{\prime}\right)
\end{equation*}
mit den Gewichten $\left\{\lambda_{i}+\lambda_{j}^{\prime} | 1 \leq i \leq n, 1 \leq j \leq n^{\prime}\right\} $ von $V \otimes V^\prime$ 
\end{remark}

\begin{remark} 
    Es gilt, dass $\rho(\mathfrak{g}_\alpha)V_\lambda \subset V_{\lambda + \alpha}$ für alle $\alpha\in\Delta$.
\end{remark}
\begin{remark}
  Weiter folgt aus Lemma \eqref{lem:diag_h} folgende Gleichheit
  \begin{equation*}
      2\frac{\lambda(H_\alpha)}{\inner{\alpha}{\alpha}}=2\frac{\inner{\lambda}{\lambda}}{\inner{\alpha}{\alpha}}\in\Z,
  \end{equation*}
  insbesondere sind also alle $\lambda\in\mathfrak{h}_0^*$ bzw. $W_\rho\subseteq\mathfrak{h}_0^*$.
\end{remark}

\begin{definition}
  Eine Linearform $\lambda \in \mathfrak{h}^*$ heisst \textbf{algebraisch ganzzahlig}, falls $\frac{2 \langle \lambda, \alpha \rangle}{\langle \alpha ,  \alpha \rangle} \in \mathbb{Z}$ für alle $\alpha \in \Delta$ gilt. Die algebraisch ganzzahligen Gewichte bilden eine diskrete Teilmenge von $\mathfrak{h}^*$, genannt \textbf{weight lattice}. (Bem: Es reicht, obige Bedingung für alle einfachen Wurzeln $\alpha\in\Pi$ zu zeigen.)
\end{definition}

\begin{definition}
  Eine Linearform $\lambda \in \mathfrak{h}^*$ heisst \textbf{dominant}, falls $\langle \lambda , \alpha \rangle \geq 0 \forall \alpha \in \Pi$. Oder äquivalenterweise für alle positiven Wurzeln.
\end{definition}

\begin{lemma}
  Das höchste Gewicht von jeder endlich dimensionalen Darstellung V ist dominant (und ganzzahlig).
\end{lemma}

\begin{remark}
  Dieses Lemma ist wichtig, da man daraus die irreduzieblen Darstellungen
  definieren kann.
\end{remark}

\begin{theorem}[Klassifikation irred. Darstellungen einer h.e. Lie Algebra]
  Sei $\mathfrak{h}$ eine halb-einfache Lie Algebra und fixiere eine Cartan-Unteralgebra $\mathfrak{h} \subset \mathfrak{g}$. Dann ist die Abbildung zwischen der Menge der Äquivalenzklassen der irreduziblen Darstellungen von $\mathfrak{g}$ und den dominanten, algebraisch ganzzahligen Linearformen $\lambda \in \mathfrak{h}_0^*$, welche jeder Darstellung ihr höchstes Gewicht zuordnet, eine Bijektion.
\end{theorem}

\begin{remark}
  Wählen wir eine Basis $(\omega_i)_i$ dual zur Basis $\{2\alpha/\inner{\alpha}{\alpha}\colon\alpha\in\Pi\}$ von $\mathfrak{h}_0^*$,
\begin{tabular}{ll}
$
      \inner{\omega_i}{\frac{2\alpha_j}{\inner{\alpha_j}{\alpha_j}}}=\delta_{ij}
$
&
\begin{tabular}{l}
Dann ist jedes ganzzahlige Element von der Form \\
$
      \sum_{j=1}^{r}n_j\omega_j,\, n_j\in\Z \quad \text{ und } \quad  r=\#\Pi
$
\end{tabular}
\end{tabular}\\
und die dominanten Elemente sind gerade diejenigen, wo $n_j\geq 0$.
\end{remark}

\begin{remark}
  Für $\mathfrak{g}=\sL2\ $ identifizieren wir $\mathfrak{h}_0^*\cong\R^{n}/\R\mathbb{I}\cong\R^{n-1}$ in der Basisdarstellung von \ref{sec:cartan_sl2c}, indem wir den letzten Eintrag auf Null setzen. Die ganzzahligen Elemente sind 
\begin{tabular}{ll}
      $\left\{\begin{bmatrix} \lambda_1 \\ \vdots \\ \lambda_{n-1} \\ 0 \end{bmatrix}
\right\}$
&
\begin{tabular}{l}
  Weiter erfüllen die dominanten Gewichte $\lambda(H_\alpha)\geq 0$
 für alle $\alpha>0$. \\ Also gilt
 $
      \lambda_1\geq\lambda_2\geq \dots\geq\lambda_{n-1}\geq 0
 $\\
für die dominanten Gewichte; insbesondere stehen die irreduziblen \\
Darstellungen von $\sL_2\ $ in 1:1 Korrespondenz zu Young-Diagrammen \\
 mit $\lambda_i$ vielen Kästchen in der $i$-ten Zeile.
\end{tabular}
\end{tabular}
\end{remark}

\begin{theorem}
  Jede endlich-dimensionale Darstellung einer halb-einfachen Lie-Algebra ist vollständig reduzibel. Das heisst isomorph zu einer direkten Summe aus irreduziblen Darstellungen $V = \bigoplus_j V_j$.
\end{theorem}

\begin{lemma}
Haben zwei Darstellungen V,W einer halbeinfachen Lie-Algebra die gleichen Gewichte (mit Vielfachheit) so gilt $V \simeq W$.
\end{lemma}