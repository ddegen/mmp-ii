\chapter{Miscellaneous}
\section{Quaternionen}
\begin{definition}
  Die \textbf{Quaternionen} sind der Schiefkörper
  \begin{equation*}
    \mathbb{H}=\{a+b i+c j+d k | a, b, c, d \in \mathbb{R}\} \cong
    \mathbb{R}^{4}
  \end{equation*}
  mit den Relationen
$
  i^{2}=j^{2}=k^{2}=-1 \quad i j=-j i=k \quad j k=-k j=i \quad k i=-i k=j
$
\end{definition}

\begin{lemma}
Die Quaternionen bilden eine $\mathbb{R}$-Algebra.
\end{lemma}

\begin{definition}
  Die \textbf{Konjugation} auf den Quaternionen ist definiert als
  \begin{equation*}
    \overline{a+b i+c j+d k}=a-b i-c j-d k
  \end{equation*}
\end{definition}

\begin{lemma}
  Für diese Konjugation gilt folgendes:

  \begin{tabular}{cc}
    $\bar{pq} = \bar{p}\bar{q} \ \forall p,q \in \mathbb{H}$ &
    $\abs{q}^2 \coloneqq q\bar{q} + \bar{q}q \geq 0 \ \forall q \in \mathbb{H}$ \\
    $\abs{q}^2 = 0 \Longleftrightarrow q=0$ &
    $\abs{pq} = \abs{p}\abs{q} \ \forall p,q \in \mathbb{H}$
  \end{tabular}
\end{lemma}
\begin{remark}
Man kann sich die (assoziative) Multiplikation der Quaternionen über Vektoren überlegen:
\begin{equation*}
p q=p_{0} q_{0}-\vec{p} \cdot \vec{q}+p_{0} \vec{q}+\vec{p} q_{0}+\vec{p} \times \vec{q}
\end{equation*}
\end{remark}

\begin{remark}
Folgende Abbildung definiert ist ein Homomorphismus von der Algebra der Quaternionen in die Algebra von $\C^{2\times2}$:
$a+bi+cj+dk \mapsto 
\begin{pmatrix} 
  a+bi & c + di \\
  -c+di & a- bi 
 \end{pmatrix}
$
\end{remark}

\section{Kugelflächenfunktionen}
Sei $P_\ell=\C[x_1,x_2,x_3]_{=\ell}$ der Raum der homogenen Polynome von Grad $\ell$ über $\C$ in drei Variablen ($\operatorname{dim}(P_l) = \frac{1}{2}(\ell+1)(\ell+2)$), sei 
\begin{equation*}
    H_\ell=\{v\in P_\ell \colon \Delta v = 0\}, \quad \operatorname{dim} H_{\ell}=2 \ell+1
\end{equation*}
der Raum der Kugelfunktionen vom Grad $\ell$. Definiere die Darstellung
\begin{equation*}
        \nu \colon O(3) \xrightarrow{}\GL(P_\ell) \qquad
        R \mapsto \left(p(x)\mapsto p(R^Tx)\right).
\end{equation*}
Wegen Rotationsinvarianz von $\Delta\colon P_\ell \xrightarrow{} P_{\ell-2}$ ($\Delta\left(p\left(R^{T} x\right)\right)=\Delta p\left(R^{T} x\right)$), welche aus der Kettenregel $\partial_i f (Ax) = \partial_k f(Ax)A_{ki}$ folgt, ist die Einschränkung von obiger Darstellung auf den Raum der Kugelfunktionen auch eine Darstellung. Bezüglich des Skalarproduktes $(,)$ definiert durch
\begin{equation*}
    (f,g)=\int_{S^2}\overline{f(x)}g(x)d^2o
\end{equation*}
sind die Darstellungen von $O(3)$ auf $P_\ell$ und somit auf $H_\ell$ unitär. Die Darstellung der Lie-Algebra $\sO (3)$ als Ableitung von $\nu$ bei der $\mathbb{1}$ ist gerade gegeben durch $\rho\defeq D_\mathbb{1}\nu$ und $\rho(L_i)=-\varepsilon_{ijk}x_j\partial_{x_k}$. Der Casimir-Operator $Z=-2(\rho(L_1)^2+\rho(L_2)^2+\rho(L_3)^2)$ auf $H_\ell$ entspricht gerade der Multiplikation mit $2\ell(\ell+1)$.

Mit der Basis von $H_\ell$ gegeben durch $p_m=r^\ell Y_{\ell m}(\theta,\phi)$ ($m=-\ell,\dots,\ell)$ in Polarkoordinaten ist der Operator $\rho(L_3)$ diagonal. Genauer gilt
\begin{equation*}
    \rho(L_3)=-imp_m
\end{equation*}
und mit den Leiteroperatoren $L_\pm=L_1\pm iL_2$ können die Kugelfunktionen $p_m$ erzeugt werden
\begin{equation*}
    p_m=c_m L_-^{\ell-m}(x_1+ix_2)^\ell,
\end{equation*}
wobei $c_m$ eine nicht-verschwindende Normalisierungskonstante ist (berechne dafür $L_+L_-p_m=(-\ell(\ell+1)+m(m-1))$ unter Verwendung der Kommutatorrelationen und des Casimir-Operators). Schliesslich folgt, dass die Darstellung $H_\ell$ irreduzibel und in der Komplexifizierung $\sO (3) \otimes \C$ isomorph zur Darstellung $V_{2\ell}$ ist.

\begin{remark}
Eine Kugelfunktion $Y\colon S^2 \xrightarrow{} \C$ vom Index $\ell$ ist die Einschränkung eines Polynoms $v\in H_\ell$ auf die 2-Sphäre $S^2$.
\end{remark}

\section{Tensorprodukt}
\begin{definition}
Das Tensorprodukt zweier Matrizen A, B ist definiert als
$A \bigotimes B =$ 
$\begin{pmatrix}
a_{11}B & \ldots & a_{1n}B \\
\vdots & \ddots & \vdots \\
a_{n1}B & \ldots & a_{nn}B
\end{pmatrix}$
und es gilt die Identität $(A_1 \bigotimes B_1)(A_2 \bigotimes B_2) = (A_1A_2)\bigotimes(B_1B_2)$.
\end{definition}{}
Für die \textbf{Zerlegung des Tensorprodukts} $V^{\bigotimes n}$ (dim(V)=m) in den symmetrischen und antisymmetrischen Teil benutzen wir:
\begin{equation*}
    dim(S^nV) = \begin{pmatrix} n+m-1 \\ n \end{pmatrix}, \qquad dim(\Lambda^nV) = \begin{pmatrix} m \\ n\end{pmatrix}
\end{equation*}
Im Spezialfall m=2 gilt der Isomorphismus $V \bigotimes V \cong S^2 V \bigoplus \Lambda ^2 V$, im Allgemeinen gilt aber $V^{\bigotimes n} \ncong S^nV \bigoplus \Lambda ^n V$. Die Aussage für m=2 lässt sich auch auf die direkte Summe zweier Vektorräume anwenden: $\left(V_{1} \oplus V_{2}\right) \otimes\left(V_{1} \oplus V_{2}\right) \simeq S^{2}\left(V_{1} \oplus V_{2}\right) \oplus \Lambda^{2}\left(V_{1} \oplus V_{2}\right)$.\\
Ebenso gilt für $V = \bigoplus_{k=1}^n n_k V_k$, dass 
\begin{equation*}
S^2V \simeq  \left( \bigoplus_{k=1}^n n_kS^2V_k \right) \oplus \left(\bigoplus_{i<j} n_in_j \left( V_i \otimes V_j \right) \right) 
\end{equation*}
