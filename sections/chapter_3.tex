\chapter{Darstellungstheorie von symmetrischen Gruppen}
\section{Konjugationsklassen}

\begin{definition}
  Wir definieren die symetrische Gruppe als die Gruppe der Automorphismen
  in der Kategorie der Mengen $S_n\defeq\aut_{\mathfrak{Set}}(\{1,\dots,n\})$.
\end{definition}

\begin{definition}
  Sei $\sigma\in S_n$.
  Die Anzahl Zykel $i_k^\sigma$ der Länge $k \in \N$ einer Permutation $\sigma$
  sind eindeutig in der disjunkten Zyklusdarstellung von $\sigma$.
\end{definition}

\begin{definition}
$\operatorname{sgn}(\sigma) = (-1)^n$ für $n \equiv$ \# Fehlstände der Permutation. 
\end{definition}

\begin{definition}
	Das \textbf{Vorzeichen} sgn($\sigma$) einer Permutation $\sigma$ ist gegeben durch $sgn(\sigma) = (-1)^{\lvert inv(\sigma) \rvert}$, wobei $inv(\sigma) = \{ (i,j) \in \{1, \ldots, n\}\times \{1, \ldots, n\} \mid i < j, \sigma(i) > \sigma(j) \}$. Eine Permutation mit Vorzeichen 1 nennen wir gerade, eine mit Vorzeichen -1 ungerade. Die Parität von $\sigma$ lässt sich bei einer Zerlegung in $k$ disjunkte Zyklen von jeweiliger Länge $m_k$ durch $sgn(\sigma)=(-1)^{m_1 + \ldots + m_k -k}$ berechnen.
\end{definition}

\begin{lemma}
  Die disjunkte Zykeldarstellung ist bis auf zyklische Permutation innerhalb
  eines Zykels und der Permutation der Zykeln eindeutig.
  Weiter gilt in der Notation von oben
\begin{equation*}
    \sum_{k=1}^\infty i_k^\sigma k = n
\end{equation*}
\end{lemma}

\begin{lemma}
Sei $\sigma \in S_n$, $\sigma=\left(i_{1} i_{2} \cdots i_{r}\right)\left(i_{r+1} \cdots\right) \cdots\left(\cdots i_{n}\right)$ so gilt $\forall \nu \in S_n$:
\begin{equation*}
\nu \sigma \nu^{-1}=\left(\nu\left(i_{1}\right) \nu\left(i_{2}\right) \cdots \nu\left(i_{r}\right)\right)\left(\nu\left(i_{r+1}\right) \cdots\right) \cdots\left(\cdots \nu\left(i_{n}\right)\right)
\end{equation*}
\end{lemma}

\begin{lemma}
  Da $\sigma_1$, $\sigma_2$ $\in S_n$ genau dann zueinander konjugiert sind,
  falls sie zerlegt in das Produkt disjunkter Zyklen denselben Zykeltyp
  aufweisen, also
  $i_k^\sigma = i_k^{\sigma^\prime} \forall k$.
  bezeichnen wir die jeweiligen \textbf{Konjugationsklasse} mit
  $C_{\textbf{i}}$ wobei $\textbf{i} = (i_1,i_2,\ldots)$.
\end{lemma}

\begin{lemma}
  Die Anzahl Partitionen p(n) einer Menge mit n Elementen entspricht
  der Anzahl an Konjugationsklassen von $S_n$ und ist bestimmt durch
  \begin{equation*}
    \sum_{n \geq 0}p(n)t^n = \prod_{k \geq 1} \frac{1}{1-t^k} 
    \quad \text{und somit zu $\mathbf{i}$} \quad
    |C_{\textbf{i}}| = \frac{n!}{\prod_{k\geq 1} \left( k^{i_k} i_k ! \right) }
    \end{equation*}
Ebenso kann man die Anzahl Paritionen schreiben als:
\begin{equation*}
p(n) = \sum_{l_1 = 0}^\infty \ldots \sum_{l_i = 0}^\infty \ldots \delta(n-1l_1 - 2l_2 - \ldots i l_i -\ldots )
\end{equation*}
\end{lemma}

\begin{definition}
  Eine \textbf{Young Tabelle} $\hat{\lambda}$ ist ein Young-Diagram $\lambda$,
  eindeutig aufgefüllt mit den Nummern von $1$ bis $n$.
  Die \textbf{Standard-Tabelle} ist die Füllung des Diagramms durch Zahlen, 
  die von Links nach Rechts
  und von Oben nach Unten in Grösse aufsteigen. 
  Nicht zu verwechseln mit dem \textbf{Standard-Schema}, welches strikt von
  links nach rechts und oben nach unten durchnummeriert. 
\end{definition}

\section{Irreduzible Darstellungen}


\begin{definition}
Die alternierende Gruppe 
$A_n := \left \{ \sigma \in S_n | sgn(\sigma) = 1 \right \}$
hat Kardinalität $n!/2$ und ist ein Normalteiler von $S_n$. Sie ist abelsch gdw. $n \leq 3$, einfach gdw. $n=5$.
\end{definition}

\begin{definition}
  Für eine Young-Tabelle $\hat{\lambda}$ bezeichnen wir mit $\hat{\lambda}^T$ dieselbe
  Tabelle an der Diagonalen gespiegelt. Zu jeder Young-Tabelle $\hat{\lambda}$
  assoziieren wir die folgende \textbf{Untergruppe} von $S_n$:
  \begin{equation*}
    G_{\hat{\lambda}} = \{ \sigma \in S_n | \sigma
    \text{ lässt die Menge der Zahlen in jeder Reihe invariant } \}
  \end{equation*}
  Das heisst, $\sigma$ angewendet auf die Tabelle bewirkt höchstens eine
  Permutation innerhalb der Reihen, aber nicht zwischen den Reihen.
  Weiter definieren wir den \textbf{Symmetrisierer} und \textbf{Anti-Symmetrisierer}:
  \begin{equation*}
    s_{\hat{\lambda}} = \sum_{\sigma \in G_{\hat{\lambda}}} \sigma, \quad
    a_{\hat{\lambda}} = \sum_{\sigma \in G_{\hat{\lambda}^T}} sgn(\sigma) \sigma \quad  \in\C[S_n] := \left \{ e_{\sigma} | \sigma \in S_n \right \}
  \end{equation*}
\end{definition}

\begin{definition}
  Das Modul $V_{\hat{\lambda}}$ zugehörig zum Young-Tableau $\hat{\lambda}$ ist die
  Darstellung $\rho_{\hat{\lambda}}: S_n \rightarrow GL(V_{\hat{\lambda}})$ von
  $S_n$ auf der Algebra $V_{\hat{\lambda}} = \mathbb{C}[S_n]s_{\hat{\lambda}} a_{\hat{\lambda}}$
  durch Links-Multiplikation, $\rho_{\hat{\lambda}}(\sigma)(vs_{\hat{\lambda}}
  a_{\hat{\lambda}})=\sigma vs_{\hat{\lambda}} a_{\hat{\lambda}}$,
  mit $\sigma \in S_n$, $v=\sum_{\sigma \in S_n} a_{\sigma} e_{\sigma} \in\C[S_n]$. Allgemein ist $V_{\lambda ^T} \cong V_{\lambda} \bigotimes sgn$ das Produkt mit der alternierenden Darstellung.
\end{definition}

\begin{example}
\begin{tabular}{ll}
\begin{tabular}{l}
In $S_3$, betrachte \\
$\hat{\lambda}_2 = \begin{ytableau} 1 & 2 \cr 3 \end{ytableau}$
\end{tabular} & $
\begin{array}{ll ll} 
G_{\hat{\lambda}_2} & = \{id, (12)\} & s_{\hat{\lambda}_2} & = e_{id}+e_{12}\\
G_{\hat{\lambda}_2}^T & = \{id, (13)\} & a_{\hat{\lambda}_2} & = e_{id}-e_{13}\\
V_{\hat{\lambda}_2} &= \C[S_n] s_{\hat{\lambda}_2} a_{\hat{\lambda}_2} & & 
\end{array}
$
\end{tabular}
\begin{eqnarray*}
s_{\hat{\lambda}_2} a_{\hat{\lambda}_2} = (e_{id}+e_{12}) (e_{id}-e_{13}) = 
e_{id} + e_{12} - e_{13} - e_{132} & := & v_1 \\
e_{23} v_1 = e_{23} + e_{132} - e_{123} - e_{12} & := & v_2
\end{eqnarray*}

Weitere, wie bspw. $e_{123}v_1=-v_2-v_1$ oder $e_{12}v_1=v_1$ erweisen sich als lin. abh..
Es gilt $V_{\hat{\lambda}_2} = span \{v_1, v_2\}$ ist nichttrivial, zweidimensional und irreduzibel.
Konkret konstruiert man $\rho_{\hat{\lambda}_2}$ in der Basis $\{v_1, v_2\}$ für die erzeugenden Transpositionen $\rho_{reg}((ij))=
  \begin{pmatrix}
      a & b \\
      c & d \\
  \end{pmatrix}$ durch Lösen des Gleichungssystems
$(e_{ij})v_1 = av_1+cv_2, (e_{ij})v_2 = bv_1+dv_2$.
$
\begin{array}{ccc}
\rho_{reg}((12))=
  \begin{pmatrix}
      1 & -1 \\
      0 & -1 \\
  \end{pmatrix}
&
\rho_{reg}((23))=
  \begin{pmatrix}
      0 & 1 \\
      1 & 0 \\
  \end{pmatrix}
&
\rho_{reg}((13))=
  \begin{pmatrix}
      -1 & 1 \\
      -1 & 0 \\
  \end{pmatrix}
\end{array}
$
\end{example}

\begin{theorem}
  Für $\lambda$ Standard-Schema sind ($V_\lambda,\rho_\lambda$) irreduzible,
  paarweise inäquivalente Darstellungen von $S_n$. Ist jedoch $\hat{\lambda}$
  ein Schema zu $\lambda$, so sind beide Darstellungen \textbf{isomorph}
  $\rho_{\hat{\lambda}}\cong\rho_\lambda$.
  Falls also $\lambda \neq \lambda^\prime$ zwei unterschiedliche
  Young-Diagramme sind, dann ist
  $V_{\hat{\lambda}}$ und $V_{\hat{\lambda}^\prime}$ nicht isomorph.
\end{theorem}

\begin{theorem}
Die symmetrische Gruppe $S_n$ hat eine kanonische Darstellung auf dem Vektorraum $V = \C_n$ durch Permutation der Basisvektoren. Diese zerfällt in die Summe zweier irreduzibler Darstellungen auf den invarianten Unterräumen $W=(1, 1, . . . , 1)$ und $W^{\perp}$.
\end{theorem}

\section{Charaktertafel}

\begin{theorem}
  \textbf{Die Frobenius Charakter Formel:}\\
  Sei $\lambda$ ein Young-Diagramm und $C_{\textbf{i}}$ eine Konjugationsklasse
  von $S_n$. So gilt:
  \begin{equation*}
    \chi_{V_{\lambda}}\left(C_{\mathrm{i}}\right)=\left(\Delta(x) \prod_{k}
    P_{k}^{i_{k}}(x)\right)_{x^{\lambda+\rho}}
  \end{equation*}
  wobei wir Multiindexnotation verwenden für
  \begin{itemize}
    \item $x = (x_1,...,x_n)$ und für einen Multiindex $\alpha =
      (\alpha_1,...,\alpha_n)$ ist $x^\alpha = x_1^{\alpha_1}\cdots x_n^{\alpha_n}$.
    \item $\Delta(x) \coloneqq \prod_{1 \leq i < k \leq n}(x_i-x_k)$ die
      \textbf{Vandermonde Determinante}
    \item $P_k(x) = x_1^k + x_2^k +...+x_n^k$ eine \textbf{Potenzsumme}
    \item Für Q(X) ein Polynom ist $(Q)_{x^a}$ der Koeffizient des Monoms
       $x^{\alpha}$,
    \item $\rho = (n-1,n-2,...,1,0)$
  \end{itemize}
  Beachte:
  \begin{enumerate}[(i)]
    \item Innerhalb des Produkts rechnen wir die gesamte Potenzsumme hoch $i_k$
      mit $p_k^{i_k}$.
    \item Das gesamte Produkt multipliziert mit der Vandermonde Determinante ist
      erneut ein Polynom, von welchem wir dann am Koeffizienten von $x^{\lambda
      + \rho}$ interessiert sind.
    \end{enumerate}
\end{theorem}

\begin{lemma}
  \textbf{Vandermonde Formel:}
  \begin{equation*}
    \operatorname{det}\left(
    \begin{pmatrix}
        x_1^{n-1} & x_1^{n-2} & \dots  & x_1    & 1\\
        x_2^{n-1} & x_2^{n-2} & \dots  & x_2    & 1\\
        \vdots    & \vdots    & \ddots & \vdots & 1\\
        x_n^{n-1} & x_n^{n-2} & \dots  & x_n    & 1
    \end{pmatrix}\right)
    =\prod_{i<j}\left(x_{i}-x_{j}\right)
  \end{equation*}
\end{lemma}

\begin{lemma}
  \textbf{Cauchy Determinanten Formel:}\\
  \begin{equation*}
  \begin{aligned}
      \operatorname{det}\left(\left[\frac{1}{x_{i}-y_{j}}\right]_{i j}\right)
      &=\frac{\prod_{i<j}\left(x_{i}-x_{j}\right)\left(y_{j}-y_{i}\right)}
      {\prod_{i,j}\left(x_{i}-y_{j}\right)}=(-1)^{n(n-1) / 2} \frac{\Delta(x)
      \Delta(y)}{\prod_{i, j}\left(x_{i}-y_{j}\right)}\\
      \operatorname{det}\left(\left[\frac{1}{1-x_{i}y_{j}}\right]_{i
     j}\right)&=\frac{\prod_{i<j}\left(x_{i}-x_{j}\right)\left(y_{j}-y_{i}\right)}
     {\prod_{i,j}\left(1-x_{i}y_{j}\right)}=\frac{\Delta(x)
     \Delta(y)}{\prod_{i, j}\left(1-x_{i}y_{j}\right)}
  \end{aligned}
  \end{equation*}
\end{lemma}

\begin{lemma}
  Für $\lambda,\lambda'$ zwei verschiedene Young-Diagramme im Standard-Schema
  und für $\xi_\lambda \coloneqq \chi_{V_\lambda}(c_{\textbf{i}})$,
  die entsprechenden Charaktere gillt:
  \begin{equation*}
    \langle \xi_\lambda , \xi_{\lambda^\prime} \rangle =
    \delta_{\lambda\lambda'}
  \end{equation*}
\end{lemma}

\begin{corollary}
  \textbf{Haken-Längen-Formel}\\
  Die Dimension der Darstellung $V_\lambda$ von $S_n$ für ein Young-Diagramm
  $\lambda$ ist gegeben durch
  \begin{equation*}
    \operatorname{dim}V_\lambda = \frac{n!}{\prod\limits_{(i,j)} h(i,j)},
  \end{equation*}
  wobei das Produkt über die Kästchen $(i,j)$ von $\lambda$ geht und $h(i,j)$
  die Hakenlänge des $(i,j)$-ten Kästchens ist.
\end{corollary}

\begin{example}
  $\boldsymbol{S_3}$:

  $\Delta(x)=\prod_{1 \leq i<j \leq 3}\left(x_{i}-x_{j}\right) = x_1^2 x_2 - x_1
  x_2^2 - x_1^2 x_3 + x_2^2 x_3 + x_1 x_3^2 - x_2 x_3^2$
  \ytableausetup{mathmode, boxsize=1em}

  \begin{tabular}{l l l}
    $3=3    $ & $i_a = (0, 0, 1, 0, \cdots)$ & $\lambda_a = (3, 0, 0) =
      \ydiagram{3}$ \\[4pt]
    $3=2+1  $ & $i_b = (1, 1, 0, 0, \cdots)$ & $\lambda_b = (2, 1, 0) =
      \ydiagram{2, 1}$ \\[8pt]
    $3=1+1+1$ & $i_c = (3, 0, 0, 0, \cdots)$ & $\lambda_c = (1, 1, 1) =
      \ydiagram{1,1,1}$ \\
  \end{tabular}

  Beachte, dass für die Charakter in folgender Formel nur die Koeffizienten
  des Polynom-Teils von entsprechendem Grad $x^{\lambda +(3,2,1,0)}$
  ermittelt werden müssen; wobei
  $r, \, s \in \{a, \, b, \,c \}$.
  \begin{multline*}
    \chi_{V_{r}}\left(C_{\mathbf{i}_{\mathbf{s}}}\right) =
    \left(\Delta(x) \prod_{k} P_{k}^{i_{k}}(x)\right)_{x^{\lambda+\rho}}
    =
    \left(
      \vphantom{
      \left(x_{1}^{1}+x_{2}^{1}+x_{3}^{1}\right)^{\left(\mathbf{i}_{\mathbf{s}}\right)_{1}}}
      \left(x_{1}-x_{2}\right) \left(x_{1}-x_{3}\right) \left(x_{2}-x_{3}\right)
      \right. \\ \left.
      \left(x_{1}^{1}+x_{2}^{1}+x_{3}^{1}\right)^{\left(\mathbf{i}_{\mathbf{s}}\right)_{1}}
      \left(x_{1}^{2}+x_{2}^{2}+x_{3}^{2}\right)^{\left(\mathbf{i}_{\mathbf{s}}\right)_{2}}
      \left(x_{1}^{3}+x_{2}^{3}+x_{3}^{3}\right)^{\left(\mathrm{i}_{\mathrm{s}}\right)_{3}}
    \right)_{x^{\lambda_r+(3,2,1,0)}}
  \end{multline*}

  \begin{tabular}{l l}
    \begin{tabular}{l}
      Die erste Spalte lässt sich über die Hacken- \\
      Längenformel ermitteln. Eine Weitere \\
      durch Anwendung obiger Frobeniusformel. \\
      Die letzte folgt aus der Orthognalität.
    \end{tabular}
    &
    \begin{tabular}{c|ccc}
       $S_3$ & $[C_{\mathrm{i}_{\mathrm{a}}}]$ & $[C_{\mathrm{i}_{\mathrm{b}}}] $
         & $[C_{\mathrm{i}_{\mathrm{c}}}]$\\
       \hline
       $V_{\lambda_a}$ & {1} & {1}  & {1} \\
       $V_{\lambda_b}$ & {2} & {-1} & {0} \\
       $V_{\lambda_c}$ & {1} & {1}  & {-1}
    \end{tabular}
  \end{tabular}
\end{example}